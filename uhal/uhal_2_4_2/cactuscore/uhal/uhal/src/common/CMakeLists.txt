include_directories($ENV{OTSDAQ_CMSTRACKER_DIR}/uhal/uhal_2_4_2/cactuscore/uhal/uhal/include)
include_directories($ENV{OTSDAQ_CMSTRACKER_DIR}/uhal/uhal_2_4_2/cactuscore/uhal/log/include)
include_directories($ENV{OTSDAQ_CMSTRACKER_DIR}/uhal/uhal_2_4_2/cactuscore/uhal/grammars/include)
include_directories($ENV{OTSDAQ_CMSTRACKER_DIR}/uhal/uhal_2_4_2/cactuscore/extern/pugixml/RPMBUILD/SOURCES/include/)

cet_set_compiler_flags(
 EXTRA_FLAGS -Wno-overloaded-virtual -Wno-unused-local-typedefs -DDISABLE_PACKET_COUNTER_HACK -DRUN_ASIO_MULTITHREADED
 )

cet_make(LIBRARY_NAME cactus_uhal_uhal
        LIBRARIES
	pthread
	${Boost_FILESYSTEM_LIBRARY}
	${Boost_SYSTEM_LIBRARY}
	${Boost_THREAD_LIBRARY}
	${Boost_REGEX_LIBRARY}
	cactus_uhal_grammars
	cactus_uhal_log
	$ENV{OTSDAQ_CMSTRACKER_DIR}/uhal/uhal_2_4_2/cactuscore/extern/pugixml/RPMBUILD/SOURCES/lib/libcactus_extern_pugixml.so
        )

install_headers()
install_source()
