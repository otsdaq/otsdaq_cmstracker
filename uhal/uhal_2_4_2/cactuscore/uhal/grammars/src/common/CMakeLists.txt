include_directories($ENV{OTSDAQ_CMSTRACKER_DIR}/uhal/uhal_2_4_2/cactuscore/uhal/grammars/include)

cet_make(LIBRARY_NAME cactus_uhal_grammars 
        LIBRARIES
	cactus_uhal_log
#	${Boost_SYSTEM_LIBRARY}
        )

install_headers()
install_source()
