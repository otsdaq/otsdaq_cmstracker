#ifndef _ots_OTDQMHistosConsumer_h_
#define _ots_OTDQMHistosConsumer_h_

#include "otsdaq-cmsoutertracker/Ph2_ACF/System/SystemController.h"
#include "otsdaq-cmsoutertracker/Ph2_ACF/DQMUtils/DQMEvent.h"
#include "otsdaq/DataManager/DQMHistosConsumerBase.h"
#include "otsdaq/Configurable/Configurable.h"
#include <string>
class TFile;
class TDirectory;

namespace ots
{

  class ConfigurationManager;
  class SLinkDQMHistogrammer;

class OTDQMHistosConsumer : public DQMHistosConsumerBase, public Configurable
{
public:
  OTDQMHistosConsumer(std::string supervisorApplicationUID, std::string bufferUID, std::string processorUID, const ConfigurationTree& theXDAQContextConfigTree, const std::string& configurationPath);
	virtual ~OTDQMHistosConsumer(void);

	void startProcessingData(std::string runNumber) override;
	void stopProcessingData (void) override;
	void pauseProcessingData (void) override;
	void resumeProcessingData(void) override;
	void     load(std::string fileName){;}

private:
	bool workLoopThread(toolbox::task::WorkLoop* workLoop);
	void fastRead(void);
	void slowRead(void);
	
	//For fast read
	std::string*                               dataP_;
	std::map<std::string,std::string>*         headerP_;

	bool                                       saveFile_; //yes or no
	std::string                                filePath_;
	std::string                                radixFileName_;
	SLinkDQMHistogrammer*                      dqmh_;
	//Ph2_System::SystemController               cSystemController;
	std::vector<uint64_t>                      cData_;
	Ph2_HwInterface::Data                      dataHelper_;
	//std::vector<Ph2_HwInterface::Event*>       eventList_;
	unsigned int                               eventDataSize_;

};
}

#endif
