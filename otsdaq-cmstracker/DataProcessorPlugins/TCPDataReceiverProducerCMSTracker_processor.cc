#include "otsdaq-cmstracker/DataProcessorPlugins/TCPDataReceiverProducerCMSTracker.h"
#include "otsdaq/Macros/CoutMacros.h"
//MORE MESSAGES?#include "otsdaq-cmstracker/Ph2_ACF/Utils/PacketHeader.h"
//MORE MESSAGES?#include "otsdaq-cmstracker/Ph2_ACF/Utils/easylogging++.h"

#include <thread>
#include <chrono>

//MORE MESSAGES? INITIALIZE_EASYLOGGINGPP

using namespace ots;

//==============================================================================
TCPDataReceiverProducerCMSTracker::TCPDataReceiverProducerCMSTracker(std::string              supervisorApplicationUID,
																	 std::string              bufferUID,
																	 std::string              processorUID,
																	 const ConfigurationTree& theXDAQContextConfigTree,
																	 const std::string&       configurationPath)
    : WorkLoop(processorUID)
	, TCPDataReceiverProducer(supervisorApplicationUID, bufferUID, processorUID, theXDAQContextConfigTree, configurationPath)
{}

//==============================================================================
TCPDataReceiverProducerCMSTracker::~TCPDataReceiverProducerCMSTracker(void) {}


//==============================================================================
bool TCPDataReceiverProducerCMSTracker::workLoopThread(toolbox::task::WorkLoop* /*workLoop*/)
{
	//std::cout << __COUT_HDR_FL__ << __PRETTY_FUNCTION__ << DataProcessor::processorUID_
	//<< " lore running, because workloop: " << WorkLoop::continueWorkLoop_ << std::endl;
	fastWrite();
	return WorkLoop::continueWorkLoop_;
}

//==============================================================================
void TCPDataReceiverProducerCMSTracker::fastWrite(void)
{
	//std::cout << __COUT_HDR_FL__ << __PRETTY_FUNCTION__ << "lore  running!" << std::endl;

	if(DataProducer::attachToEmptySubBuffer(dataP_, headerP_) < 0)
	{
		__COUT__ << "There are no available buffers! Retrying...after waiting 10 milliseconds!" << std::endl;
		std::this_thread::sleep_for(std::chrono::microseconds(1000));
		return;
	}

	try
	{
		if(dataType_ == "Packet")
			*dataP_ = TCPSubscribeClient::receivePacket();         // Throws an exception if it fails
		else//"Raw" || DEFAULT
		{   
			*dataP_ = TCPSubscribeClient::receive<std::string>();  // Throws an exception if it fails
		}
		if(dataP_->size() == 0)  // When it goes in timeout
			return;
	}
	catch(const std::exception& e)
	{
		//std::cout << __LINE__ << __PRETTY_FUNCTION__ << "LORENZO Error: " << e.what() << std::endl;
		if(std::string(e.what()).find("Connection closed") != std::string::npos)
		{
		    //std::cout << __LINE__ << __PRETTY_FUNCTION__ << "Disconnecting client..." << std::endl;
			TCPSubscribeClient::disconnect();
			//This is not the proper thing to do but I need to wait until the server is up again
			//std::this_thread::sleep_for(std::chrono::milliseconds(1000));
		    //std::cout << __LINE__ << __PRETTY_FUNCTION__ << "Trying to reconnect client..." << std::endl;
			TCPSubscribeClient::connect(30, 1000);
		    //std::cout << __LINE__ << __PRETTY_FUNCTION__ << "Client reconnected..." << std::endl;
			TCPSubscribeClient::setReceiveTimeout(1, 0);
		}
		//std::this_thread::sleep_for(std::chrono::milliseconds(500));
		return;
	}

	(*headerP_)["IPAddress"] = ipAddress_;
	(*headerP_)["Port"]      = std::to_string(port_);

	//MAYBE WE CAN THINK OF SENDING MORE MESSAGES TO LET THIS PRODUCER KNOW THE STATUS OF THE PH2_ACF...
	//SO I AM GOING TO LEAVE THIS CODE HERE COMMENTED AND ALSO THE INCLUDES AND STUFF COMMENTED

	// //Checking for ENDOFSTREAM to reconnect
	// PacketHeader thePacketHeader;
    // uint8_t packerHeaderSize = thePacketHeader.getPacketHeaderSize();
	// fDataBuffer.insert(fDataBuffer.end(), dataP_->begin(), dataP_->end());
	// while (fDataBuffer.size() > 0)
	// {
	// 	// std::cout << "fDataBuffer size: " << fDataBuffer.size() << std::endl;
	// 	if (fDataBuffer.size() < packerHeaderSize)
	// 	{
	// 		break; // Not enough bytes to retreive the packet size
	// 	}
	// 	uint32_t packetSize = thePacketHeader.getPacketSize(fDataBuffer);
	// 	if (fDataBuffer.size() < packetSize)
	// 	{
	// 		break; // Packet not completed, waiting
	// 	}
	// 	std::string inputStream(fDataBuffer.begin() + packerHeaderSize, fDataBuffer.begin() + packetSize);
	// 	fDataBuffer.erase(fDataBuffer.begin(), fDataBuffer.begin() + packetSize);

	// 	if(inputStream == END_OF_TRANSMISSION_MESSAGE)
	// 	{
	// 		DataProducer::setWrittenSubBuffer<std::string, std::map<std::string, std::string>>();
	// 		std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << "MESSAGE: " << END_OF_TRANSMISSION_MESSAGE << ". Trying to reconnect to server!" << __E__;
	// 		std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << "MESSAGE: " << END_OF_TRANSMISSION_MESSAGE << ". Trying to reconnect to server!" << __E__;
	// 		std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << "MESSAGE: " << END_OF_TRANSMISSION_MESSAGE << ". Trying to reconnect to server!" << __E__;
	// 		// TCPSubscribeClient::disconnect();
	// 		// TCPSubscribeClient::connect(1200, 1000);
	// 		// std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << "MESSAGE: " << END_OF_TRANSMISSION_MESSAGE << ". Connected to server!" << __E__;
	// 		// std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << "MESSAGE: " << END_OF_TRANSMISSION_MESSAGE << ". Connected to server!" << __E__;
	// 		// std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << "MESSAGE: " << END_OF_TRANSMISSION_MESSAGE << ". Connected to server!" << __E__;
	// 		// TCPSubscribeClient::setReceiveTimeout(1, 0);
	// 		return;
	// 	}
	// }
	DataProducer::setWrittenSubBuffer<std::string, std::map<std::string, std::string>>();
}

DEFINE_OTS_PROCESSOR(TCPDataReceiverProducerCMSTracker)
