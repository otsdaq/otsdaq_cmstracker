#ifndef _ots_TCPDataReceiverProducerCMSTracker_h_
#define _ots_TCPDataReceiverProducerCMSTracker_h_

#include "otsdaq/DataProcessorPlugins/TCPDataReceiverProducer.h"
#include "otsdaq/Macros/ProcessorPluginMacros.h"
#include <vector>


namespace ots
{
class ConfigurationTree;

class TCPDataReceiverProducerCMSTracker : public TCPDataReceiverProducer
{
  public:
	TCPDataReceiverProducerCMSTracker(std::string              supervisorApplicationUID,
	                        		  std::string              bufferUID,
	                        		  std::string              processorUID,
	                        		  const ConfigurationTree& theXDAQContextConfigTree,
	                        		  const std::string&       configurationPath);
	virtual ~TCPDataReceiverProducerCMSTracker(void);
	// virtual void startProcessingData(std::string runNumber) override;
	// virtual void stopProcessingData(void) override;
	protected:
	bool workLoopThread(toolbox::task::WorkLoop* workLoop) override;
	void fastWrite(void);	
  private:
	std::vector<char> fDataBuffer;

};

}  // namespace ots

#endif
