#include "otsdaq-cmstracker/DataProcessorPlugins/DQMMiddlewareHistosConsumer.h"
#include "otsdaq/MessageFacility/MessageFacility.h"
#include "otsdaq/XmlUtilities/HttpXmlDocument.h"
#include "otsdaq/Macros/CoutMacros.h"
#include "otsdaq/Macros/ProcessorPluginMacros.h"
#include "otsdaq-cmstracker/Ph2_ACF/Utils/easylogging++.h"
#include "otsdaq-cmstracker/Ph2_ACF/Utils/ConfigureInfo.h"
#include "otsdaq-cmstracker/Ph2_ACF/Parser/FileParser.h"
#include "otsdaq-cmstracker/Ph2_ACF/DQMUtils/DQMInterface.h"
#include "otsdaq-cmstracker/Ph2_ACF/DQMUtils/DQMCalibrationFactory.h"
#include "otsdaq-cmstracker/Ph2_ACF/Utils/ContainerSerialization.h"

#include <TDirectory.h>
#include <TFile.h>
#include <TTree.h>
#include <TGraph.h>
#include <TH1.h>
#include <TKey.h>
//#include <TROOT.h>

#include <chrono>
#include <thread>

INITIALIZE_EASYLOGGINGPP

#define TIME_FORMAT_FOR_DIRECTORY "%Y-%m-%d_%H-%M-%S"

using namespace ots;

//========================================================================================================================
DQMMiddlewareHistosConsumer::DQMMiddlewareHistosConsumer(std::string supervisorApplicationUID, std::string bufferUID, std::string processorUID, const ConfigurationTree &theXDAQContextConfigTree, const std::string &configurationPath)
	: WorkLoop                    (processorUID)
	, DQMHistosConsumerBase       (supervisorApplicationUID, bufferUID, processorUID, LowConsumerPriority)
	, Configurable                (theXDAQContextConfigTree, configurationPath)
	, saveFile_                   (theXDAQContextConfigTree_.getNode(configurationPath).getNode("SaveFile").getValue<bool>())
	, filePath_                   (theXDAQContextConfigTree_.getNode(configurationPath).getNode("FilePath").getValue<std::string>())
	, radixFileName_              (theXDAQContextConfigTree_.getNode(configurationPath).getNode("RadixFileName").getValue<std::string>())
	, calibrationName_            (theXDAQContextConfigTree_.getNode(configurationPath).getNode("CalibrationName").getValue<std::string>())
	, configurationDir_           (theXDAQContextConfigTree_.getNode(configurationPath).getNode("ConfigurationDir").getValue<std::string>())
	, configurationName_          (theXDAQContextConfigTree_.getNode(configurationPath).getNode("ConfigurationName").getValue<std::string>())
	, configurationFilePath_      (configurationDir_ + "/" + configurationName_)
	, moduleConfigurationFileName_("")
//, dqmh_                (0)

{
	// configure the logger
    el::Configurations conf(std::string(getenv("PH2ACF_BASE_DIR")) + "/settings/logger.conf");
	std::cout << __PRETTY_FUNCTION__ << "Log conf: " << std::string(getenv("PH2ACF_BASE_DIR")) + "/settings/logger.conf" << std::endl;
    el::Loggers::reconfigureAllLoggers(conf);
	//	gStyle->SetPalette(1);
}

//========================================================================================================================
DQMMiddlewareHistosConsumer::~DQMMiddlewareHistosConsumer(void)
{
	// DQMHistosBase::closeFile();
}
//========================================================================================================================
void DQMMiddlewareHistosConsumer::startProcessingData(std::string runNumber)
{

	std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << "Start" << std::endl;
	std::string firmwareName;
	std::string readoutFrequency;
	std::string modulesType;
	std::string calibrationName = calibrationName_;

	ConfigureInfo theConfigureInfo;
	try
	{
		moduleConfigurationFileName_ = __ENV__("MODULE_NAMES_FILE");
	}
	catch (std::runtime_error &e)
	{
		// If there is no environment, proceed without.
		// In fact MODULE_NAME_FILE must only be declared if the burnin is in use, and there the environment is checked if it exists
		std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << "The following is a WARNING not an error. "
					<< "It just means that there is likely no file and it is not necessary to read it. "
					   "The file is necessary when running the burnin box but this might not be the case. ->"
					<< e.what() << __E__;
		moduleConfigurationFileName_ = "";
	}

	if (moduleConfigurationFileName_ == "")
	{
		theConfigureInfo.setConfigurationFiles(configurationFilePath_);
	}
	else 
	{
		HttpXmlDocument cfgXml;		
		if (cfgXml.loadXmlDocument(moduleConfigurationFileName_))
		{
			std::vector<std::string> moduleLocation;
			std::vector<std::string> moduleId;
			std::vector<std::string> moduleName;
			cfgXml.getAllMatchingValues("ModuleLocation", moduleLocation);
			cfgXml.getAllMatchingValues("ModuleId", moduleId);
			cfgXml.getAllMatchingValues("ModuleName", moduleName);
			for (unsigned i = 0; i < moduleName.size(); i++)
			{
				//std::cout << __LINE__ << "] " << __PRETTY_FUNCTION__ << moduleLocation[i] << " : " << atoi(moduleId[i].c_str()) << " : " << moduleName[i] << std::endl;
				if (moduleName[i] != "Empty")
					theConfigureInfo.enableOpticalGroup(0, atoi(moduleId[i].c_str()), moduleName[i]);
			}
			configurationFilePath_ = StringMacros::convertEnvironmentVariables(cfgXml.getMatchingValue("ConfigurationFile", 0).c_str());
			// If there is the configuration file then we use the xml defined in the Burninbox GUI
			//configurationFilePath_ = configurationFilePath_+".NISER"; 
			theConfigureInfo.setConfigurationFiles(configurationFilePath_);
			calibrationName = cfgXml.getMatchingValue("ModulesType", 0) + calibrationName_;

			std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << "ConfigurationFileName: " << configurationFilePath_ << std::endl;

 		}
		else
		{
			std::stringstream ss;
			ss << "Error: Failed to properly load file " + moduleConfigurationFileName_ + ". Make sure it exists and it well formatted. Try to write it again." << std::endl;
			throw std::runtime_error(ss.str());
		}
	}

	theConfigureInfo.setCalibrationName(calibrationName);

    Ph2_Parser::FileParser theFileParser;
    std::stringstream      out;
	Ph2_Parser::SettingsMap fSettingsMap;

    CommunicationSettingConfig theCommunicationSettingConfig;
    theFileParser.parseCommunicationSettings(configurationFilePath_, theCommunicationSettingConfig, out);
    theFileParser.parseHW(configurationFilePath_, &fDetectorStructure, out);
   	theFileParser.parseSettings(configurationFilePath_, fSettingsMap, out);
    theConfigureInfo.setEnabledObjects(&fDetectorStructure);

	DQMHistosBase::openFile(filePath_ + "/" + radixFileName_ + runNumber + ".root");
    DQMCalibrationFactory theDQMCalibrationFactory;
    fDQMHistogrammerVector = theDQMCalibrationFactory.createDQMHistogrammerVector(calibrationName);
	// DQMHistosBase::myDirectory_ = DQMHistosBase::theFile_->mkdir("OuterTracker", "OuterTracker");
	// DQMHistosBase::myDirectory_->cd();

	for (auto dqmHistogrammer : fDQMHistogrammerVector)
		dqmHistogrammer->book(theFile_, fDetectorStructure, fSettingsMap);

	fDataBuffer.clear();

	iterationCounter_ = -1;

	DataConsumer::startProcessingData(runNumber);
	std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << "StartDone" << std::endl;
}

//========================================================================================================================
void DQMMiddlewareHistosConsumer::stopProcessingData(void)
{
	if (fDataBuffer.size() > 0)
	{
		//std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << "\tBuffer should be empty, some data were not read, Aborting " << std::endl;
		std::stringstream ss;
		ss << "Error: The DQMMiddlewareHistosConsumer buffer should be empty, some data were not read!" << std::endl;
		throw std::runtime_error(ss.str());
	}

	{
		std::unique_lock<std::mutex> lock(DQMHistosConsumerBase::fillHistoMutex_);
		for (auto dqmHistogrammer : fDQMHistogrammerVector)
			dqmHistogrammer->process();
	}

	DataConsumer::stopProcessingData();
	if (saveFile_)
	{
		DQMHistosBase::save();
	}
	closeFile();
	for (auto dqmHistogrammer : fDQMHistogrammerVector)
		delete dqmHistogrammer;
	fDQMHistogrammerVector.clear();
}

//========================================================================================================================
void DQMMiddlewareHistosConsumer::pauseProcessingData(void)
{
	DataConsumer::stopProcessingData();
}

//========================================================================================================================
void DQMMiddlewareHistosConsumer::resumeProcessingData(void)
{
	DataConsumer::startProcessingData("");
}

//========================================================================================================================
bool DQMMiddlewareHistosConsumer::workLoopThread(toolbox::task::WorkLoop *workLoop)
{
	//	std::cout << __PRETTY_FUNCTION__ << DataProcessor::processorUID_ << " running, because workloop: " << WorkLoop::continueWorkLoop_ << std::endl;
	fastRead();
	return WorkLoop::continueWorkLoop_;
}

//========================================================================================================================
void DQMMiddlewareHistosConsumer::fastRead(void)
{
	//std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << "TRYING TO GET DATA!" << std::endl;
	if (DataConsumer::read(dataP_, headerP_) < 0)
	{
		std::this_thread::sleep_for(std::chrono::milliseconds(100));
		return;
	}
	//std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << "GOT DATA AND Start Processing buffer block." << std::endl;
	//std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << "GOT DATA AND Start Processing buffer block." << std::endl;
	//std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << "GOT DATA AND Start Processing buffer block." << std::endl;

	if (histogramsToReset_ == true)
	{
		theFile_->cd("Detector");
		TDirectory *sourceDir = gDirectory->CurrentDirectory();
		ResetDir(sourceDir);
		histogramsToReset_ = false;
	}

    PacketHeader thePacketHeader;
    uint8_t packerHeaderSize = thePacketHeader.getPacketHeaderSize();
	//std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << "Got Something" << std::endl;
	fDataBuffer.insert(fDataBuffer.end(), dataP_->begin(), dataP_->end());
	while (fDataBuffer.size() > 0)
	{
		//std::cout << "fDataBuffer size: " << fDataBuffer.size() << std::endl;
		if (fDataBuffer.size() < packerHeaderSize)
		{
			std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << " " << __LINE__ << std::endl;
			break; // Not enough bytes to retreive the packet size
		}
		uint32_t packetSize = thePacketHeader.getPacketSize(fDataBuffer);
		//std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << " Packet Number received " << int(theCurrentStream->getPacketNumber()) << std::endl;
		//std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << " vector size " << fDataBuffer.size() << "; expected " << packetSize << std::endl;
		if (fDataBuffer.size() < packetSize)
		{
			//std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << "Packet not completed waiting" << std::endl;
			break; // Packet not completed, waiting
		}
		std::string inputStream(fDataBuffer.begin() + packerHeaderSize, fDataBuffer.begin() + packetSize);
		fDataBuffer.erase(fDataBuffer.begin(), fDataBuffer.begin() + packetSize);

		if(inputStream == END_OF_TRANSMISSION_MESSAGE)
		{
			std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << "\tMESSAGE: " << END_OF_TRANSMISSION_MESSAGE << ". Filling and saving histos!" << std::endl;
			theFile_->cd("Detector");
			TDirectory *sourceDir = gDirectory->CurrentDirectory();
			TDirectory *destinationDir;
			std::string dirName;
			if (iterationCounter_ == -1)
			{
				dirName = "Iteration_start_";
			}
			else
			{
				dirName = "Iteration_" + std::to_string(iterationCounter_) + "_";
			}
			dirName += currentDateTime();
			destinationDir = theFile_->mkdir(dirName.c_str());
			destinationDir->cd();
			if(fDQMHistogrammerVector.size() > 0)
			{
				std::unique_lock<std::mutex> lock(DQMHistosConsumerBase::fillHistoMutex_);
				for (auto dqmHistogrammer : fDQMHistogrammerVector)
				{
					dqmHistogrammer->process();
				}			
			}
			CopyDir(sourceDir);
			if (saveFile_)
			{
				std::unique_lock<std::mutex> lock(DQMHistosConsumerBase::fillHistoMutex_);
				std::cout << std::endl;
				DQMHistosBase::autoSave(true);
			}

			histogramsToReset_ = true;

			DataConsumer::setReadSubBuffer<std::string, std::map<std::string, std::string>>();
			++iterationCounter_;
			//std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << ": Done Processing buffer block." << std::endl;
			return;
		}
		else
		{
			bool decodedByOneDQM = false;
			for (auto dqmHistogrammer : fDQMHistogrammerVector)
			{
				std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << " " << currentDateTime() << "\tFilling" << std::endl;
				if (dqmHistogrammer->fill(inputStream))
				{
					decodedByOneDQM = true;
					std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << " " << currentDateTime() << "\tDone Filling" << std::endl;
					break;
				}
			}
			if(!decodedByOneDQM)
			{
				std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << " " << currentDateTime() << "\tNo DQM histogrammer decoded message " << inputStream << ", aborting..." << RESET;
				exit(0);
			}
		}
	}

	DataConsumer::setReadSubBuffer<std::string, std::map<std::string, std::string>>();
	//std::cout << __LINE__ << __PRETTY_FUNCTION__ << ": Done Processing buffer block." << std::endl;
}

// void DQMMiddlewareHistosConsumer::CopyDir(TDirectory *source)
// {
// 	//copy all objects and subdirs of directory source as a subdir of the current directory
// 	source->ls();
// 	TDirectory *saveDir = gDirectory;
// 	TDirectory *aDir = saveDir->mkdir(source->GetName());
// 	aDir->cd();
// 	//loop on all entries of this directory
// 	// TKey *key;
// 	// TIter nextkey(source->GetListOfKeys());
// 	// while ((key = (TKey *)nextkey()))
// 	TList *MyList = source->GetList();
// 	TIter next(MyList);
// 	TObject *key;
// 	while((key = (TObject*)next()))
// 	{
// 		//const char *classname = key->GetClassName();
// 		//TClass *cl = gROOT->GetClass(classname);
// 		// if (!cl)
// 		// {
// 		// 	continue;
// 		// }
// 		if (key->InheritsFrom(TDirectory::Class()))
// 		{
// 			source->cd(key->GetName());
// 			TDirectory *subdir = gDirectory;
// 			aDir->cd();
// 			CopyDir(subdir);
// 			aDir->cd();
// 		}
// 		else if (key->InheritsFrom(TTree::Class()))
// 		{
// 			TTree *T = (TTree *)source->Get(key->GetName());
// 			aDir->cd();
// 			TTree *newT = T->CloneTree(-1, "fast");
// 			newT->Write();
// 		}
// 		else
// 		{
// 			// source->cd();
// 			// TObject *obj = key->ReadObject();
// 			std::cout << key << std::endl;
// 			aDir->cd();
// 			key->Write();
// 			if(key->InheritsFrom(TGraph::Class()))
// 			{
// 				std::cout << "Found tgraph " << std::endl;
// 				//static_cast<TGraph*>(key)->Set(0);
// 			}
// 			else if(key->InheritsFrom(TH1::Class()))
// 				static_cast<TH1*>(key)->Reset();
// 			else
// 			{
// 				std::cout << "Don't know which object you are talking about!" << std::endl;
// 			}

// //			delete obj;
// 		}
// 	}
// 	aDir->SaveSelf(kTRUE);
// 	saveDir->cd();
// }

//========================================================================================================================
// void DQMMiddlewareHistosConsumer::CopyDir(TDirectory *source)
// {
// 	// copy all objects and subdirs of directory source as a subdir of the current directory
// 	//source->ls();
// 	std::cout<< __PRETTY_FUNCTION__ << " [" << __LINE__ << "]" 
// 	<< "SOURCE DIR: " << source->GetName() 
// 	<< "GLOBAL DIR: " << gDirectory->GetName() 
// 	<< std::endl;
// 	TDirectory *saveDir = gDirectory;
// 	TDirectory *aDir = saveDir->mkdir(source->GetName());
// 	aDir->cd();
// 	// loop on all entries of this directory
// 	//  TKey *key;
// 	//  TIter nextkey(source->GetListOfKeys());
// 	//  while ((key = (TKey *)nextkey()))
// 	TList *MyList = source->GetList();
// 	TIter next(MyList);
// 	TObject *key;
// 	while ((key = (TObject *)next()))
// 	{
// 		// const char *classname = key->GetClassName();
// 		// TClass *cl = gROOT->GetClass(classname);
// 		//  if (!cl)
// 		//  {
// 		//  	continue;
// 		//  }
// 		if (key->InheritsFrom(TDirectory::Class()))
// 		{
// 			std::cout<< __PRETTY_FUNCTION__ << " [" << __LINE__ << "]" << "Directory: " << key->GetName() << std::endl;
// 			source->cd(key->GetName());
// 			TDirectory *subdir = gDirectory;
// 			aDir->cd();
// 			CopyDir(subdir);
// 			aDir->cd();
// 		}
// 		else if (key->InheritsFrom(TTree::Class()))
// 		{
// 			std::cout<< __PRETTY_FUNCTION__ << " [" << __LINE__ << "]" << "TREE" << std::endl;
// 			TTree *T = (TTree *)source->Get(key->GetName());
// 			aDir->cd();
// 			TTree *newT = T->CloneTree(-1, "fast");
// 			newT->Write();
// 		}
// 		else
// 		{
// 			//source->cd();
// 			//TObject *obj = key->ReadObj();
// 			//std::cout<< __PRETTY_FUNCTION__ << " [" << __LINE__ << "]" << "DIR: " << aDir->GetName() << " KEY: " << key->GetName() << std::endl;
// 			aDir->cd();
// 			key->Write();
// 		}
// 	}
// 	aDir->SaveSelf(kTRUE);
// 	saveDir->cd();
// }

//========================================================================================================================
void DQMMiddlewareHistosConsumer::CopyDir(TDirectory* source)
{
    TDirectory* savdir = gDirectory;
    TDirectory* adir   = savdir->mkdir(source->GetName());
    adir->cd();
    TList *theListOfObjects = source->GetList();
    TList *theListOfKeys = source->GetListOfKeys();
    std::map<std::string, TObject*> theMapOfObjects; // because vector are better then TList
    for(int i=0; i<theListOfKeys->GetSize(); ++i) 
    {
        theMapOfObjects[theListOfKeys->At(i)->GetName()] = theListOfKeys->At(i);
    }
    for(int i=0; i<theListOfObjects->GetSize(); ++i)// Object should come after to save the most recent plot (TObject) and not the one saved in the file (TKey)
    {
        theMapOfObjects[theListOfObjects->At(i)->GetName()] = theListOfObjects->At(i);
    }

    for(auto theInputNameAndObject : theMapOfObjects)
    {
        const char* classname = theInputNameAndObject.second->ClassName();
        //std::cout<< __PRETTY_FUNCTION__ << " [" << __LINE__ << "] Class Name = " << classname << std::endl;
        //std::cout<< __PRETTY_FUNCTION__ << " [" << __LINE__ << "] Object Name = " << theInputNameAndObject.first << std::endl;
        // TClass* cl = gROOT->GetClass(classname);
        // if(!cl) continue;
        if(theInputNameAndObject.second->InheritsFrom("TDirectory"))
        {
            source->cd(theInputNameAndObject.first.c_str());
            TDirectory* subdir = gDirectory;
            adir->cd();
            CopyDir(subdir);
            adir->cd();
        }
        else if(theInputNameAndObject.second->InheritsFrom("TTree"))
        {
            TTree* T = (TTree*)source->Get(theInputNameAndObject.first.c_str());
            adir->cd();
            TTree* newT = T->CloneTree();
            newT->Write();
        }
        else if(theInputNameAndObject.second->InheritsFrom("TKey"))
        {
            // source->cd();
            auto theInputKey = static_cast<TKey*>(theInputNameAndObject.second);
            //std::cout << "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! Copying key " << theInputNameAndObject.first << std::endl;
            TObject* obj = theInputKey->ReadObj();
            // std::cout << theInputNameAndObject.first << " : " << adir->GetName() << std::endl;
            adir->cd();
            obj->Write(theInputNameAndObject.first.c_str());
            delete obj;
            obj = nullptr;
        }
        else
        {
            //std::cout << "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! Copying object " << theInputNameAndObject.first << std::endl;
            adir->cd();
            theInputNameAndObject.second->Write();
        }
    }
    adir->SaveSelf(kTRUE);
    savdir->cd();
}

//========================================================================================================================
void DQMMiddlewareHistosConsumer::ResetDir(TDirectory *source)
{
	TList *MyList = source->GetList();
	TIter next(MyList);
	TObject *key;
	while ((key = (TObject *)next()))
	{
		//std::cout << "Name: " << key->GetName() << std::endl;
        if (key->InheritsFrom(TDirectory::Class()))
		{
			source->cd(key->GetName());
			TDirectory *subdir = gDirectory;
			ResetDir(subdir);
		}
		else if (key->InheritsFrom(TTree::Class()))
		{
			TTree *T = (TTree *)source->Get(key->GetName());
			T->Reset();
		}
        else if (key->InheritsFrom(TGraph::Class()))
        {
            //std::cout << "Found TGraph " << std::endl;
            // static_cast<TGraph*>(key)->Set(0);
        }
        else if (key->InheritsFrom(TCanvas::Class()))
        {
            //std::cout << "Found TCanvas " << std::endl;
            // Retrieve the list of primitives (objects) from the canvas
            TList *primitives = static_cast<TCanvas*>(key)->GetListOfPrimitives();

            // Find the TGraph in the list of primitives
            TGraph *retrievedGraph = nullptr;
            TObject *obj;
            TIter next(primitives);
            while ((obj = next())) {
                if (obj->InheritsFrom(TGraph::Class())) {
                    retrievedGraph = (TGraph*)obj;
                    retrievedGraph->Set(0);
                    break;
                }
            }
        }
        else if (key->InheritsFrom(TH1::Class()))
            static_cast<TH1 *>(key)->Reset();
		else
		{
			std::cout << "Don't know which object you are talking about: " << key->GetName() << std::endl;
		}
	}
}

//========================================================================================================================
const std::string DQMMiddlewareHistosConsumer::currentDateTime()
{
	time_t now = time(0);
	struct tm tstruct;
	char buf[80];
	tstruct = *localtime(&now);
	strftime(buf, sizeof(buf), TIME_FORMAT_FOR_DIRECTORY, &tstruct);

	return buf;
}

DEFINE_OTS_PROCESSOR(DQMMiddlewareHistosConsumer)
