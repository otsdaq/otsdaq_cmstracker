#ifndef _ots_FEPowerSupplyInterface_h_
#define _ots_FEPowerSupplyInterface_h_

#include "otsdaq/FECore/FEVInterface.h"
#include "otsdaq/NetworkUtilities/TCPPublishServer.h"
#include "otsdaq-cmstracker/OTUtilities/PowerSupplyData.h"
#include "pugixml.hpp"

#include <string>

class PowerSupply;
class PowerSupplyChannel;

namespace ots
{
	class FEPowerSupplyInterface : public FEVInterface
	{
	public:
		FEPowerSupplyInterface(const std::string &interfaceUID, const ConfigurationTree &theXDAQContextConfigTree, const std::string &configurationPath);
		virtual ~FEPowerSupplyInterface(void);

		void configure(void);
		void halt(void);
		void pause(void);
		void resume(void);
		void start(std::string runNumber) override;
		void stop(void);

		bool running(void);

		void universalRead(char *address, char *readValue) override { ; }
		void universalWrite(char *address, char *writeValue) override { ; }

		// MACROS
		void turnOnMacro(__ARGS__);
		void turnOffMacro(__ARGS__);
		void doIVCurves(__ARGS__);
		void powerCycle(__ARGS__);

	private:
		TCPPublishServer  dataPublisher_;
		const std::string unknownTypeName = "UNKNOWN";
		const std::string lvTypeName = "LV";
		const std::string hvTypeName = "HV";
		bool isChannelOn(PowerSupplyChannel *channel);
		bool turnOnChannel(PowerSupplyChannel *channel);
		bool turnOffChannel(PowerSupplyChannel *channel);
		bool turnOn(void);
		bool turnOff(void);

		void startStopTimer(void);

		std::vector<std::string> getListOfOnChannels(void);
		std::vector<std::string> getListOfOffChannels(void);
		struct ChannelPair
		{
			PowerSupplyChannel *channel = nullptr;
			PowerSupplyChannel *channelToWaitFor = nullptr;
		};
		class ChannelInfo
		{
		public:
			ChannelInfo() {};
			ChannelInfo(PowerSupplyChannel *aChannel, std::string aType, float aSetVoltage, bool use)
				: channel(aChannel), type(aType), setVoltage(aSetVoltage), inUse(use)
			{
			}
			PowerSupplyChannel *channel = nullptr;
			std::string type = "UNKNOWN";
			float setVoltage = 0;
			bool inUse = false;
		};

		std::map<std::string, bool> locationMapFromConfig_;
		std::map<std::string, std::map<unsigned, ChannelPair>> turnOnSequenceMap_; // maps for turning on and off sequence, and the configured channels, as well as the powersupplies used.
		std::map<std::string, std::map<unsigned, ChannelPair>> turnOffSequenceMap_;
		//       PowerSupply           Channel
		std::map<std::string, std::map<std::string, ChannelInfo>> activeChannelsMap_;
		//std::map<std::string, std::map<std::string, PowerSupplyChannel *>> configuredChannelsMap_;

		std::map<std::string, PowerSupply *> powerSupplyMap_;
		PowerSupplyData thePowerSupplyStatusData_;
		pugi::xml_document powerSupplyDocument_;
		bool doneStartStop_ = false;
		void printTime(const std::string& message = "");
	};
} // namespace ots
#endif