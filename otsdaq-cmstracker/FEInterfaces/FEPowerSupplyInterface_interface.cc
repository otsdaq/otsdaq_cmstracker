#include "otsdaq/MessageFacility/MessageFacility.h"
#include "otsdaq/Macros/CoutMacros.h"
#include "otsdaq/Macros/InterfacePluginMacros.h"
#include "otsdaq-cmstracker/FEInterfaces/FEPowerSupplyInterface.h"
#include "otsdaq/XmlUtilities/HttpXmlDocument.h"

#ifdef __CAEN__
#include "otsdaq-cmstracker/power_supply/src/CAEN.h" //This is done to prevent an error in compilation about windows.h
#endif
#include "otsdaq-cmstracker/power_supply/src/Keithley.h"
#include "otsdaq-cmstracker/power_supply/src/RohdeSchwarz.h"
#include "otsdaq-cmstracker/power_supply/src/TTi.h"
#include "otsdaq-cmstracker/power_supply/src/CAENelsFastPS.h"

#include <chrono>
#include <thread>
#include <future>

using namespace ots;

//========================================================================================================================
FEPowerSupplyInterface::FEPowerSupplyInterface(const std::string &interfaceUID, const ConfigurationTree &theXDAQContextConfigTree, const std::string &configurationPath)
	: FEVInterface(interfaceUID, theXDAQContextConfigTree, configurationPath)
	, dataPublisher_(theXDAQContextConfigTree.getNode(configurationPath).getNode("DataPublisherPort").getValue<unsigned int>(),1)
	, powerSupplyDocument_()
{
	// MACROS REGISTRATION
	////////////////////////////////////////////////////////////////////////////////////
	FEVInterface::registerFEMacroFunction(
		"TurnOnPowerSupplies",																 // feMacroName
		static_cast<FEVInterface::frontEndMacroFunction_t>(&FEPowerSupplyInterface::turnOnMacro), // feMacroFunction
		std::vector<std::string>{},															 //"},        // namesOfInputArgs
		std::vector<std::string>{},															 // namesOfOutputArgs
		1);																					 // requiredUserPermissions

	////////////////////////////////////////////////////////////////////////////////////
	FEVInterface::registerFEMacroFunction(
		"TurnOffPowerSupplies",																  // feMacroName
		static_cast<FEVInterface::frontEndMacroFunction_t>(&FEPowerSupplyInterface::turnOffMacro), // feMacroFunction
		std::vector<std::string>{},															  //"},        // namesOfInputArgs
		std::vector<std::string>{},															  // namesOfOutputArgs
		1);																					  // requiredUserPermissions

	////////////////////////////////////////////////////////////////////////////////////
	FEVInterface::registerFEMacroFunction(
		"PowerCycle",																  // feMacroName
		static_cast<FEVInterface::frontEndMacroFunction_t>(&FEPowerSupplyInterface::powerCycle), // feMacroFunction
		std::vector<std::string>{},															  //"},        // namesOfInputArgs
		std::vector<std::string>{},															  // namesOfOutputArgs
		1);																					  // requiredUserPermissions

	////////////////////////////////////////////////////////////////////////////////////
	FEVInterface::registerFEMacroFunction(
		"DoIVCurves",																										   // feMacroName
		static_cast<FEVInterface::frontEndMacroFunction_t>(&FEPowerSupplyInterface::doIVCurves),							   // feMacroFunction
		std::vector<std::string>{"VoltageBegin", "VoltageEnd", "VoltageStep", "WaitSecondsBeforeReading", "NumberOfReadings"}, //"},        // namesOfInputArgs
		std::vector<std::string>{},																							   // namesOfOutputArgs
		1);																													   // requiredUserPermissions

	dataPublisher_.startAccept();
	std::cout << __PRETTY_FUNCTION__ << "ConstructorDone!!!" << std::endl;
}

//========================================================================================================================
FEPowerSupplyInterface::~FEPowerSupplyInterface(void)
{
}

//========================================================================================================================
void FEPowerSupplyInterface::configure(void)
{
	std::cout << __PRETTY_FUNCTION__ << "Configure!" << std::endl;
	powerSupplyMap_.clear();//All power supplies and their channels configured AND InUse 

	activeChannelsMap_.clear();//All Channels in the power supply map that are turned on and off in the PowerSupplyToDetectorTable 
	turnOnSequenceMap_.clear();//Map for turning on the channels
	turnOffSequenceMap_.clear();//Map for turning off the channels

	// pugi::xml_document* doc = new pugi::xml_document(); // creates xml document and all of its components
	pugi::xml_node deviceNode = powerSupplyDocument_.append_child("Devices");
	pugi::xml_node powerSupplyNode;
	pugi::xml_node channelNode;

	for (auto &device : theXDAQContextConfigTree_.getNode(theConfigurationPath_ + "/LinkToPowerSupplyDeviceTable").getChildren())
	{
		if (theXDAQContextConfigTree_.getNode(theConfigurationPath_ + "/LinkToPowerSupplyDeviceTable/" + device.second.getValueAsString() + "/LinkToPowerSupplyTable/Status").getValue() == "Off")
			continue;

		powerSupplyNode = deviceNode.append_child("PowerSupply");
		powerSupplyNode.append_attribute("ID").set_value(device.first.c_str()); // needs to use c string because of the definition on the function that does not accept strings
		for (auto &powerSupply : theXDAQContextConfigTree_.getNode(theConfigurationPath_ + "/LinkToPowerSupplyDeviceTable/" + device.second.getValueAsString() + "/LinkToPowerSupplyTable").getChildren())
		{
			
			if (powerSupply.first == "Status")
			{
				powerSupplyNode.append_attribute("InUse").set_value("Yes");
			}
			else if (powerSupply.first == "LinkToPowerSupplyChannelTable")
			{
				for (auto &channelGroup : theXDAQContextConfigTree_.getNode(theConfigurationPath_ + "/LinkToPowerSupplyDeviceTable/" + device.second.getValueAsString() + "/LinkToPowerSupplyTable/LinkToPowerSupplyChannelTable/").getChildren())
				{
					//std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << "Channel: " << channelGroup.second.getValueAsString() << " InUse? " << channelGroup.second.getNode("InUse").getValue() << std::endl;
					if (channelGroup.second.getNode("InUse").getValue().compare("No") == 0 || channelGroup.second.getNode("InUse").getValue().compare("no") == 0)
						continue;
					channelNode = powerSupplyNode.append_child("Channel");
					channelNode.append_attribute("ID").set_value(channelGroup.second.getValueAsString().c_str());
					for (auto &channel : theXDAQContextConfigTree_.getNode(theConfigurationPath_ + "/LinkToPowerSupplyDeviceTable/" + device.second.getValueAsString() + "/LinkToPowerSupplyTable/LinkToPowerSupplyChannelTable/" + channelGroup.second.getValueAsString()).getChildren())
					{
						// eliminates unwanted sections of the table
						if (channel.first != "LinkToPowerSupplyChannelTable" && channel.first != "CommentDescription" && channel.first != "Author" && channel.first != "RecordInsertionTime" && channel.first != "PowerSupplyGroupUID")
						{
							channelNode.append_attribute((channel.first).c_str()).set_value(channel.second.getValueAsString().c_str());
						}
					}
				}
			}
			else if (powerSupply.first != "CommentDescription" && powerSupply.first != "Author" && powerSupply.first != "RecordInsertionTime")
			{
				powerSupplyNode.append_attribute((powerSupply.first).c_str()).set_value(powerSupply.second.getValueAsString().c_str()); // appends values on powersupplynode
			}
		}
	}
	//std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << "Saving Configuration: " << powerSupplyDocument_.save_file("save_file_output.xml") << std::endl;
	try
	{																																		   // saves the result for checking purposes
		for (pugi::xml_node powerSupply = deviceNode.child("PowerSupply"); 
		powerSupply; 
		powerSupply = powerSupply.next_sibling("PowerSupply")) // checks for the type of PowerSupply
		{
			std::string inUse = powerSupply.attribute("InUse").value();
			if (inUse.compare("No") == 0 || inUse.compare("no") == 0)
				continue;
			std::string id = powerSupply.attribute("ID").value();
			std::string model = powerSupply.attribute("Model").value();
			if (model.compare("CAENelsFastPS") == 0)
			{
				powerSupplyMap_.emplace(id, new CAENelsFastPS(powerSupply));
			}
#ifdef __CAEN__
			else if (model.compare("CAEN") == 0)
			{
				std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << "Creating CAEN" << std::endl;
				powerSupplyMap_.emplace(id, new CAEN(powerSupply));
				std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << "Done creating CAEN" << std::endl;
			}
#endif
			else if (model.compare("TTi") == 0)
			{
				powerSupplyMap_.emplace(id, new TTi(powerSupply));
			}
			else if (model.compare("RohdeSchwarz") == 0)
			{
				powerSupplyMap_.emplace(id, new RohdeSchwarz(powerSupply));
			}
			else if (model.compare("Keithley") == 0)
			{
				powerSupplyMap_.emplace(id, new Keithley(powerSupply));
			}
			else
			{
				std::stringstream error;
				error << "The Model: " << model
					  << " is not an available power supply and won't be initialized, "
						 "please check the xml configuration file.";
				std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << error.str() << std::endl;
				throw std::runtime_error(error.str());
			}
		}
	}
	catch (std::exception &e)
	{
		std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << "THROWING AN EXCEPTION: " << e.what() << std::endl;
		__FE_SS__ << e.what() << __E__;
		__FE_SS_THROW__;
	}

    //Adding all channels that are in use to the monitoring!
	for (auto &powerSupply : powerSupplyMap_)
	{
		for (auto &powerSupplyChannelName : powerSupply.second->getChannelList())
		{
			thePowerSupplyStatusData_.addChannel(powerSupply.first, powerSupplyChannelName);
		}
	}
	//All the other maps are derived from the PowerSupplyToDetectorTable AND the CHANNEL NAME IS UNIQUE (It doesn't matter which power supply it belongs to!)
	bool channelFound = false;
	for (auto &channel : theXDAQContextConfigTree_.getNode(theConfigurationPath_ + "/LinkToPowerSupplyToDetectorTable").getChildren())
	{
		std::string channelUID = channel.second.getValueAsString();

		std::string group                    = theXDAQContextConfigTree_.getNode(theConfigurationPath_ + "/LinkToPowerSupplyToDetectorTable/" + channelUID + "/ChannelGroup").getValue();
		bool        turnOnOff                = theXDAQContextConfigTree_.getNode(theConfigurationPath_ + "/LinkToPowerSupplyToDetectorTable/" + channelUID + "/TurnOnOff").getValue<bool>();
		std::string type                     = theXDAQContextConfigTree_.getNode(theConfigurationPath_ + "/LinkToPowerSupplyToDetectorTable/" + channelUID + "/ChannelType").getValue<std::string>();
		uint        turnOnSequence           = theXDAQContextConfigTree_.getNode(theConfigurationPath_ + "/LinkToPowerSupplyToDetectorTable/" + channelUID + "/TurnOnSequence").getValue<unsigned>();
		std::string turnOnWaitForChannelUID  = theXDAQContextConfigTree_.getNode(theConfigurationPath_ + "/LinkToPowerSupplyToDetectorTable/" + channelUID + "/TurnOnWaitForChannelUID").getValue();
		uint        turnOffSequence          = theXDAQContextConfigTree_.getNode(theConfigurationPath_ + "/LinkToPowerSupplyToDetectorTable/" + channelUID + "/TurnOffSequence").getValue<unsigned>();
		std::string turnOffWaitForChannelUID = theXDAQContextConfigTree_.getNode(theConfigurationPath_ + "/LinkToPowerSupplyToDetectorTable/" + channelUID + "/TurnOffWaitForChannelUID").getValue();
		std::string channelLinkUID           = theXDAQContextConfigTree_.getNode(theConfigurationPath_ + "/LinkToPowerSupplyToDetectorTable/" + channelUID + "/LinkToChannelUID").getValue();

		if(!turnOnOff)	continue;
		
		channelFound = false;
		for (auto &powerSupply : powerSupplyMap_)
		{
			for (auto &powerSupplyChannelName : powerSupply.second->getChannelList())
			{
				//std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << "Channel:  " << channelName << std::endl;
				if (powerSupplyChannelName == channelLinkUID)
				{
					channelFound = true;
					if (turnOnSequenceMap_[group].find(turnOnSequence) != turnOnSequenceMap_[group].end())
					{
						__FE_SS__ << "Channel group " << group << " has more than 1 element with turnOn sequence equal to " << turnOnSequence << __E__;
						__FE_SS_THROW__;
					}
					else
					{
						ChannelPair aPair; // Both are initialized with nullptr
						aPair.channel = powerSupply.second->getChannel(powerSupplyChannelName);
						//std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << "Sequence: " << turnOnSequence << " Channel p:  " << aPair.channel << std::endl;
						if (turnOnWaitForChannelUID != "NONE")
						{
							try
							{
								aPair.channelToWaitFor = powerSupply.second->getChannel(turnOnWaitForChannelUID); // Will throw an exception if not found
							}
							catch (std::exception &e)
							{
								__FE_SS__ << e.what() << __E__;
								__FE_SS_THROW__;
							}
						}
						else
						{
							aPair.channelToWaitFor = nullptr;
						}
						turnOnSequenceMap_[group][turnOnSequence] = aPair;
						activeChannelsMap_[group][aPair.channel->getID()] = ChannelInfo(aPair.channel, type, aPair.channel->getParameterFloat("V0Set"), true);
					}

					if (turnOffSequenceMap_[group].find(turnOffSequence) != turnOffSequenceMap_[group].end())
					{
						__FE_SS__ << "Channel group " << group << " has more than 1 element with turnOff sequence equal to " << turnOffSequence << __E__;
						__FE_SS_THROW__;
					}
					else
					{
						ChannelPair aPair; // Both are initialized with nullptr
						aPair.channel = powerSupply.second->getChannel(powerSupplyChannelName);
						if (turnOffWaitForChannelUID != "NONE")
						{
							try
							{
								aPair.channelToWaitFor = powerSupply.second->getChannel(turnOffWaitForChannelUID); // Will throw an exception if not found
							}
							catch (std::exception &e)
							{
								__FE_SS__ << e.what() << __E__;
								__FE_SS_THROW__;
							}
						}
						else
						{
							aPair.channelToWaitFor = nullptr;
						}
						turnOffSequenceMap_[group][turnOffSequence] = aPair;
					}
				}
				if(channelFound) break;
			}
			if(channelFound) break;
		}
		if(!channelFound)
		{
			__FE_SS__ << "Can NOT find channel " << link << " in the power supply channel table." << __E__;
			__FE_SS_THROW__;
		}
	}

	std::string moduleConfigurationFileName_;
	try
	{
		moduleConfigurationFileName_ = __ENV__("MODULE_NAMES_FILE");
	}
	catch (std::runtime_error &e)
	{
		// If there is no environment, proceed without.
		// In fact MODULE_NAME_FILE must only be declared if the burnin is in use, and there the environment is checked if it exists
		__FE_COUT__ << "The following is a WARNING not an error. "
					<< "It just means that there is likely no file and it is not necessary to read it. "
					   "The file is necessary when running the burnin box but this might not be the case. ->"
					<< e.what() << __E__;
		moduleConfigurationFileName_ = "";
	}

	if (moduleConfigurationFileName_ != "")
	{
		HttpXmlDocument cfgXml;
		if (cfgXml.loadXmlDocument(moduleConfigurationFileName_))
		{
			std::vector<std::string> moduleLocation;
			std::vector<std::string> moduleId;
			std::vector<std::string> moduleName;
			cfgXml.getAllMatchingValues("ModuleLocation", moduleLocation);
			cfgXml.getAllMatchingValues("ModuleId", moduleId);
			cfgXml.getAllMatchingValues("ModuleName", moduleName);

			// Removing elements from the turnOnSequence if they are not configured in the Burnin
			for (auto &channelGroup : turnOnSequenceMap_) // turns on based on the sequence determined on the config table, usually first LV1, then HV1, LV2, HV2, etc.
			{
				std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << "Removing: " << channelGroup.first << std::endl; // saves the result for checking purposes
				for (auto it = channelGroup.second.cbegin(), nextIt = it; it != channelGroup.second.cend(); it = nextIt)
				{
					++nextIt;
					bool found = false;
					for (uint i = 0; i < moduleName.size(); i++)
					{
						if ((moduleName[i] != "Empty") && it->second.channel->getID().find(moduleLocation[i]) == 0) // Module location must be found at the beginning of the name
						{
							std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << "Channel: " << it->second.channel->getID() << " has been found at: " << moduleLocation[i] << std::endl;
							found = true;
							break;
						}
					}
					if (!found)
					{
						channelGroup.second.erase(it);
						auto delIt = activeChannelsMap_[channelGroup.first].find(it->second.channel->getID());
						if(delIt != activeChannelsMap_[channelGroup.first].end())
						{
							activeChannelsMap_[channelGroup.first].erase(delIt);
						}
					}
				}
				std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << "Channels left" << std::endl;
				for (auto &sequence : channelGroup.second)
				{
					std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << "Channel left: " << sequence.second.channel->getID() << std::endl;
				}
				std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << "Done Channels left" << std::endl;
			}

			// Removing elements from the turnOffSequence if they are not configured in the Burnin
			for (auto &channelGroup : turnOffSequenceMap_) // turns on based on the sequence determined on the config table, usually first LV1, then HV1, LV2, HV2, etc.
			{
				std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << "Removing: " << channelGroup.first << std::endl; // saves the result for checking purposes
				for (auto it = channelGroup.second.cbegin(), nextIt = it; it != channelGroup.second.cend(); it = nextIt)
				{
					++nextIt;
					bool found = false;
					for (uint i = 0; i < moduleName.size(); i++)
					{
						if ((moduleName[i] != "Empty") && it->second.channel->getID().find(moduleLocation[i]) == 0) // Module location must be found at the beginning of the name
						{
							std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << "Channel: " << it->second.channel->getID() << " has been found at: " << moduleLocation[i] << std::endl;
							found = true;
							break;
						}
					}
					if (!found)
					{
						channelGroup.second.erase(it);
					}
				}
				std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << "Channels left" << std::endl;
				for (auto &sequence : channelGroup.second)
				{
					std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << "Channel left: " << sequence.second.channel->getID() << std::endl;
				}
				std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << "Done Channels left" << std::endl;
			}
		}
		else
		{
			__FE_SS__ << "Error: Failed to properly use file " + moduleConfigurationFileName_ + ". Make sure it exists and it well formatted. Try to write it again." << __E__;
			__FE_SS_THROW__;
		}
	}
	// Check if the turn ON and turn OFF sequences are consistent, which means that the channels that I will turn on they will also be turned off
	for (auto &channelGroup : turnOnSequenceMap_)
	{
		if (turnOffSequenceMap_.find(channelGroup.first) == turnOffSequenceMap_.end())
		{
			__FE_SS__ << "Error: The channel group " << channelGroup.first << " is configured in the turn ON sequence but it is not configured in the turn OFF sequence." << __E__;
			__FE_SS_THROW__;
		}
		for (auto &sequenceOn : channelGroup.second)
		{
			bool found = false;
			for (auto &sequenceOff : turnOffSequenceMap_[channelGroup.first])
			{
				if (sequenceOff.second.channel->getID() == sequenceOn.second.channel->getID())
				{
					found = true;
					break;
				}
			}
			if (!found)
			{
				__FE_SS__ << "Error: The channel ID " << sequenceOn.second.channel->getID() << " is configured in the turn ON sequence but it is not configured in the turn OFF sequence." << __E__;
				__FE_SS_THROW__;
			}
		}
	}
	if (moduleConfigurationFileName_ != "")
	{
		try
		{
			turnOn();
		}
		catch (const std::exception &e)
		{
			__FE_SS__ << e.what() << __E__;
			std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << e.what() << std::endl;
			__FE_SS_THROW__;
		}
	}
}

//========================================================================================================================
void FEPowerSupplyInterface::halt(void)
{
	std::thread t(&FEPowerSupplyInterface::startStopTimer, this);

	try
	{
		turnOff();
	}
	catch (const std::exception &e)
	{
		doneStartStop_ = true;
		t.join();
		std::vector<std::string> badChannels = getListOfOffChannels();
		if (badChannels.size() == 0)
		{
			__FE_SS__ << e.what() << __E__;
			std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << e.what() << std::endl;
			__FE_SS_THROW__;
		}
		else
		{
			__FE_SS__ << "Can NOT turn off channel: ";
			for (auto channel : badChannels)
				ss << channel << "\n";
			ss << __E__;
			std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << ss.str() << std::endl;
			__FE_SS_THROW__;
		}
	}

	t.join();
}

//========================================================================================================================
void FEPowerSupplyInterface::pause(void)
{
}

//========================================================================================================================
void FEPowerSupplyInterface::resume(void)
{
}

//========================================================================================================================
void FEPowerSupplyInterface::start(std::string runNumber)
{
	std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << "START" << std::endl; // saves the result for checking purposes

	doneStartStop_ = false;
	
	std::thread t(&FEPowerSupplyInterface::startStopTimer, this);

	try
	{
		turnOn();
	}
	catch (const std::exception &e)
	{
		doneStartStop_ = true;
		t.join();
		std::vector<std::string> badChannels = getListOfOffChannels();
		if (badChannels.size() == 0)
		{
			__FE_SS__ << e.what() << __E__;
			std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << e.what() << std::endl;
			__FE_SS_THROW__;
		}
		else
		{
			__FE_SS__ << "Can NOT turn on channel: ";
			for (auto channel : badChannels)
				ss << channel << "\n";
			ss << __E__;
			std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << ss.str() << std::endl;
			__FE_SS_THROW__;
		}
	}
	doneStartStop_ = true;
	t.join();
	std::cout << __LINE__ << "]" << __PRETTY_FUNCTION__ << "START DONE" << std::endl;
}

//========================================================================================================================
// The running state is a thread
bool FEPowerSupplyInterface::running(void)
{
	//std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << "Running" << std::endl;
	printTime("FEPowerSupplyInterface::running() Workloop: " + std::to_string(WorkLoop::continueWorkLoop_));
	for (auto &powerSupply : powerSupplyMap_)
	{
		for (auto &powerSupplyChannelName : powerSupply.second->getChannelList())
		{
			PowerSupplyChannel* channel = powerSupply.second->getChannel(powerSupplyChannelName);
			const int nAllowedAttempts = 10;
			for (int attempt = 0; attempt < nAllowedAttempts; attempt++)
			{
				try
				{
					thePowerSupplyStatusData_.setChannelStatus(powerSupply.first, powerSupplyChannelName, channel->getCurrent(), channel->getVoltage(), channel->isOn(), time(NULL));
					break;
				}
				catch (const std::exception &e)
				{
					std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << "Power supply channel with ID: " << channel->getID() << " had an exception: " << e.what() << " at attempt #: " << attempt << ". Retrying..." << std::endl;
					std::this_thread::sleep_for(std::chrono::milliseconds(1000));
				}
			}
		}
	}
	dataPublisher_.broadcastPacket(thePowerSupplyStatusData_.convertToJson(PowerSupplyData::StatusDataName)); // broadcast over tcp and goes to a consumer.
	std::this_thread::sleep_for(std::chrono::milliseconds(5000));
	return WorkLoop::continueWorkLoop_; // otherwise it stops!!!!!
}

//========================================================================================================================
void FEPowerSupplyInterface::stop(void)
{
	doneStartStop_ = false;
	std::cout << __LINE__ << "] " << __PRETTY_FUNCTION__ << "\tSTOP. Workloop: " << WorkLoop::continueWorkLoop_ << std::endl; // saves the result for checking purposes
	printTime("STOPING");
	printTime("STOP DONE");
	std::cout << __LINE__ << "] " << __PRETTY_FUNCTION__ << "\tSTOP DONE" << std::endl;
	doneStartStop_ = true;
}

//========================================================================================================================
bool FEPowerSupplyInterface::turnOn(void)
{
	std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << "Turning on!" << std::endl;
	for (auto &channelGroup : turnOnSequenceMap_) // turns on based on the sequence determined on the config table, usually first LV, then HV
	{
		for (auto &sequence : channelGroup.second)
		{
			if (!isChannelOn(sequence.second.channel))
			{
				if (sequence.second.channelToWaitFor == nullptr)
				{
					turnOnChannel(sequence.second.channel);
				}
				else
				{
					turnOnChannel(sequence.second.channelToWaitFor);
					turnOnChannel(sequence.second.channel);
				}
			}
			// Not sure if this is actually necessary...if it doesn't turn on there is an exception so inUse will always be true...
			activeChannelsMap_[channelGroup.first][sequence.second.channel->getID()].inUse = true;
		}
	}
	std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << "Done turning on!" << std::endl;

	return true;
}

//========================================================================================================================
bool FEPowerSupplyInterface::turnOff(void)
{
	for (auto &channelGroup : turnOffSequenceMap_) // turns off based on the sequence determined on the config table, usually first HV, then LV
	{
		for (auto &sequence : channelGroup.second)
		{
			if (isChannelOn(sequence.second.channel))
			{
				if (sequence.second.channelToWaitFor == nullptr)
				{
					turnOffChannel(sequence.second.channel);
				}
				else
				{
					turnOffChannel(sequence.second.channelToWaitFor);
					turnOffChannel(sequence.second.channel);
				}
			}
		}
	}
	return true;
}

//========================================================================================================================
void FEPowerSupplyInterface::startStopTimer(void)
{
	const int timeout = 30;
	const int exceptionTimeout = 100;
	const int checkTimeout = 1; // seconds
	int timer = 0;
	unsigned int iterationIndex = 0;

	while (!doneStartStop_)
	{
		std::this_thread::sleep_for(std::chrono::seconds(checkTimeout));
		timer += checkTimeout;
		if (timer > timeout)
		{
			// iterationIndex = VStateMachine::getIterationIndex();
			// std::cout << "IterationIndex: " << iterationIndex << std::endl;
			// if(iterationIndex > exceptionTimeout)
			// {
			// 	//Not sure an exception is the right thing in a thread
			// 	// __FE_SS__ << "Error: I have spent already " << timeout*exceptionTimeout << " seconds waiting in this state." << std::endl;
			// 	// __FE_SS_THROW__;
			// 	std::cout << "Error: I have spent already " << timeout*exceptionTimeout << " seconds waiting in this state." << std::endl;
			// 	break;
			// }
			timer = 0;
			VStateMachine::indicateIterationWork();
		}
	}
	std::cout << __LINE__ << "]" << __PRETTY_FUNCTION__ << "Timer done" << std::endl;
}

//========================================================================================================================
bool FEPowerSupplyInterface::turnOnChannel(PowerSupplyChannel *channel)
{
	const int timeout = 30;		// 60 seconds
	const int checkTimeout = 1; // seconds
	const int nAllowedAttempts = 2;

	int timer = timeout;
	for (int attempt = 0; attempt < nAllowedAttempts; attempt++)
	{
		timer = timeout;
		try
		{
			// Turning channel on
			channel->turnOn();

			// If the set voltage is 0 return
			if (channel->getSetVoltage() == 0)
				return true;

			// Checking that the channel actually turns on
			while ((!channel->isOn() || (1 - (channel->getOutputVoltage() / channel->getSetVoltage()) > 0.05)) && timer > 0) // 5% tolerance...
			{
				std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << "ID: " << channel->getID() << " is on? " << channel->isOn() << " voltage: " << channel->getOutputVoltage() << std::endl;
				std::this_thread::sleep_for(std::chrono::seconds(checkTimeout));
				timer -= checkTimeout;
			}
			if (timer > 0)
				return true;
		}
		catch (const std::exception &e)
		{
			std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << "ID: " << channel->getID() << " had an exception: " << e.what() << " at attempt #: " << attempt << ". Retrying..." << std::endl;
			std::this_thread::sleep_for(std::chrono::milliseconds(1000));
		}
	}
	// The channel didn't turn on so throwing an exception
	if (timer < 0)
	{
		__FE_SS__ << "Error: Can NOT turn ON channel " << channel->getID() << " after " << timeout << " seconds. Turning the channel OFF. The status of the channel before turning it OFF was: " << (channel->isOn() ? "ON" : "OFF") << ". The set voltage was: " << channel->getSetVoltage() << ". The output voltage was: " << channel->getOutputVoltage() << __E__;
		std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << "Error: Can NOT turn ON channel " << channel->getID() << " after " << timeout << " seconds. Turning the channel OFF. The status of the channel before turning it OFF was: " << (channel->isOn() ? "ON" : "OFF") << ". The set voltage was: " << channel->getSetVoltage() << ". The output voltage was: " << channel->getOutputVoltage() << std::endl;
		try
		{
			turnOffChannel(channel);
		}
		catch (std::exception &e)
		{
			// do nothing and throw the turn on exception
		}
		__FE_SS_THROW__;
	}
	return true;
}

//========================================================================================================================
bool FEPowerSupplyInterface::turnOffChannel(PowerSupplyChannel *channel)
{
	const int timeout = 30;		// 60 seconds
	const int checkTimeout = 1; // seconds

	const int nAllowedAttempts = 2;

	int timer = timeout;
	for (int attempt = 0; attempt < nAllowedAttempts; attempt++)
	{
		timer = timeout;
		try
		{
			// Turning channel off
			channel->turnOff();

			// If the set voltage is 0 return
			if (channel->getSetVoltage() == 0)
				return true;

			// Checking that the channel actually turns on
			while ((channel->isOn() || channel->getOutputVoltage() > channel->getSetVoltage() / 100.) && timer > 0) // 1% tolerance...
			{
				std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << "ID: " << channel->getID() << " is on? " << channel->isOn() << " voltage: " << channel->getOutputVoltage() << std::endl;
				std::this_thread::sleep_for(std::chrono::seconds(checkTimeout));
				timer -= checkTimeout;
			}
			if (timer > 0)
				return true;
		}
		catch (const std::exception &e)
		{
			std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << "ID: " << channel->getID() << " had an exception: " << e.what() << " at attempt #: " << attempt << ". Retrying..." << std::endl;
			std::this_thread::sleep_for(std::chrono::milliseconds(1000));
		}
	}

	// The channel didn't turn off so throwing an exception
	if (timer < 0)
	{
		__FE_SS__ << "Can NOT turn OFF channel " << channel->getID() << " after " << timeout << " seconds. The status of the channel is: " << (isChannelOn(channel) ? "ON" : "OFF") << ". The set voltage is: " << channel->getSetVoltage() << ". The output voltage is: " << channel->getOutputVoltage() << __E__;
		__FE_SS_THROW__;
	}
	return true;
}

//========================================================================================================================
std::vector<std::string> FEPowerSupplyInterface::getListOfOnChannels(void)
{
	std::vector<std::string> channels;
	for (auto channelGroup : turnOnSequenceMap_) // turns on based on the sequence determined on the config table, usually first LV1, then HV1, LV2, HV2, etc.
	{
		for (auto sequence : channelGroup.second)
		{
			if (isChannelOn(sequence.second.channel))
			{
				channels.push_back(sequence.second.channel->getID());
			}
		}
	}
	return channels;
}

//========================================================================================================================
std::vector<std::string> FEPowerSupplyInterface::getListOfOffChannels(void)
{
	std::vector<std::string> channels;
	for (auto channelGroup : turnOffSequenceMap_) // turns on based on the sequence determined on the config table, usually first LV1, then HV1, LV2, HV2, etc.
	{
		for (auto sequence : channelGroup.second)
		{
			if (isChannelOn(sequence.second.channel))
			{
				channels.push_back(sequence.second.channel->getID());
			}
		}
	}
	return channels;
}

//========================================================================================================================
bool FEPowerSupplyInterface::isChannelOn(PowerSupplyChannel *channel)
{
	const int nAllowedAttempts = 10;
	int attempt = 1;
	for (; attempt <= nAllowedAttempts; attempt++)
	{
		try
		{
			return channel->isOn();
		}
		catch (const std::exception &e)
		{
			std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << "ID: " << channel->getID() << " had an exception: " << e.what() << " at attempt #: " << attempt << ". Retrying..." << std::endl;
			if (attempt == nAllowedAttempts)
			{
				throw std::runtime_error(e.what());
			}
			std::this_thread::sleep_for(std::chrono::milliseconds(1000));
		}
	}
}

//========================================================================================================================
void FEPowerSupplyInterface::turnOnMacro(__ARGS__)
{
	try
	{
		turnOn();
	}
	catch (const std::exception &e)
	{
		std::vector<std::string> badChannels = getListOfOffChannels();
		if (badChannels.size() == 0)
		{
			__FE_SS__ << e.what() << __E__;
			std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << e.what() << std::endl;
			__FE_SS_THROW__;
		}
		else
		{
			__FE_SS__ << "Can NOT turn on channel: ";
			for (auto channel : badChannels)
				ss << channel << "\n";
			ss << __E__;
			std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << ss.str() << std::endl;
			__FE_SS_THROW__;
		}
	}
}

//========================================================================================================================
void FEPowerSupplyInterface::turnOffMacro(__ARGS__)
{
	try
	{
		turnOff();
	}
	catch (const std::exception &e)
	{
		std::vector<std::string> badChannels = getListOfOffChannels();
		if (badChannels.size() == 0)
		{
			__FE_SS__ << e.what() << __E__;
			std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << e.what() << std::endl;
			__FE_SS_THROW__;
		}
		else
		{
			__FE_SS__ << "Can NOT turn off channel: ";
			for (auto channel : badChannels)
				ss << channel << "\n";
			ss << __E__;
			std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << ss.str() << std::endl;
			__FE_SS_THROW__;
		}
	}
}

//========================================================================================================================
void FEPowerSupplyInterface::doIVCurves(__ARGS__)
{
	std::cout << "[" << __LINE__ << "]" << "void FEPowerSupplyInterface::doIVCurves(__ARGS__)" << "Staring IV!" << std::endl;
	// std::vector<std::string>{"VoltageBegin", "VoltageEnd", "VoltageStep", "WaitSecondsBeforeReading", "NumberOfReadings"},															  //"},        // namesOfInputArgs
	//__FE_COUT__ << "Begin Macro DoIVCurve!!!!" << __E__;
	
	//First check if the channels are on and if not, right now, throw an exception
	for (auto &channelGroup : activeChannelsMap_) // turns on based on the sequence determined on the config table, usually first LV, then HV
	{
		//std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << "ChannelInfo: " << channelGroup.second.size() << std::endl;
		for (auto &channelInfo : channelGroup.second)
		{
			PowerSupplyChannel *channel = channelInfo.second.channel;
			if (!channelInfo.second.inUse || channelInfo.second.type.compare(hvTypeName) != 0)
				continue;
			if (!isChannelOn(channel))
			{
				__FE_SS__ << "Error: Channel " << channel->getID() << " must be on if you want to do an IV curve." << __E__;
				std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << "Error: Channel " << channel->getID() << " must be on if you want to do an IV curve." << std::endl;
				__FE_SS_THROW__;
			}
		}
	}
	dataPublisher_.broadcastPacket(PowerSupplyData::BeginIVCurve);
	for (auto &argIn : argsIn)
		std::cout << __PRETTY_FUNCTION__ << argIn.first << ": " << argIn.second << __E__;

	float pVoltageBegin = __GET_ARG_IN__("VoltageBegin", float);
	float pVoltageEnd = __GET_ARG_IN__("VoltageEnd", float);
	float pVoltageStep = __GET_ARG_IN__("VoltageStep", float);
	uint pWaitSecondsBeforeReading = __GET_ARG_IN__("WaitSecondsBeforeReading", uint);
	uint pNumberOfReadings = __GET_ARG_IN__("NumberOfReadings", uint);

	std::cout << "[" << __LINE__ << "]" << "void FEPowerSupplyInterface::doIVCurves(__ARGS__)"
			  << "VoltageBegin: " << pVoltageBegin
			  << " VoltageEnd: " << pVoltageEnd
			  << " VoltageStep: " << pVoltageStep
			  << " Wait: " << pWaitSecondsBeforeReading
			  << " Readings: " << pNumberOfReadings
			  << std::endl;

//	std::cout << "[" << __LINE__ << "]" << "void FEPowerSupplyInterface::doIVCurves(__ARGS__)" << "Staring IV!" << std::endl;

	const int timeout = 60;		// 60 seconds
	const int checkTimeout = 1; // seconds
								//	const int nAllowedAttempts = 10;

	int timer = timeout;
	//	uint totalSteps = fabs(pVoltageEnd-pVoltageBegin)/fabs(pVoltageStep);
	float voltageStep = 0;

	(pVoltageEnd - pVoltageBegin >= 0) ? voltageStep = std::fabs(pVoltageStep) : voltageStep = -std::fabs(pVoltageStep);

	std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << "Active channels: " << activeChannelsMap_.size() << std::endl;
	for (float voltage = pVoltageBegin; std::fabs(voltage) <= std::fabs(pVoltageEnd); voltage += voltageStep)
	{
		std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << "Voltage: " << voltage << std::endl;
		PowerSupplyData ivCurvePowerSupplyData;
		
		for (auto &channelGroup : activeChannelsMap_) // turns on based on the sequence determined on the config table, usually first LV, then HV
		{
			//std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << "ChannelInfo: " << channelGroup.second.size() << std::endl;
			for (auto &channelInfo : channelGroup.second)
			{
				PowerSupplyChannel *channel = channelInfo.second.channel;
				if (!channelInfo.second.inUse || channelInfo.second.type.compare(hvTypeName) != 0)
					continue;
				int maxNumberOfExceptions = 5;
				while(maxNumberOfExceptions > 0)
				{
					try
					{
						//std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << "Channel: " << channel->getID() << " SettingVoltage: " << voltage << std::endl;
						channel->setVoltage(voltage);
						break;//SUCCESS
					}
					catch (std::exception &e)
					{
						//std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << "Channel: " << channel->getID() << " Voltage: " << voltage << " maxNumberOfExceptions: " << maxNumberOfExceptions << " Exception: " << e.what() << std::endl;
						//Try maxNumberOfExceptions times and if the voltage can't be set just throw an exception
						if(maxNumberOfExceptions > 0)
						{
							--maxNumberOfExceptions;
						}
						else
						{
							__FE_SS__ << "Error: Can NOT set voltage: " << voltage << " for channel: " << channel->getID() << " after " << timer << " seconds. Turning the channel OFF. The status of the channel before turning it OFF was: " << (channel->isOn() ? "ON" : "OFF") << ". The set voltage was: " << channel->getSetVoltage() << ". The output voltage was: " << channel->getOutputVoltage() << __E__;
							std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << "Error: Can NOT set voltage: " << voltage << " for channel: " << channel->getID() << " after " << timer << " seconds. Turning the channel OFF. The status of the channel before turning it OFF was: " << (channel->isOn() ? "ON" : "OFF") << ". The set voltage was: " << channel->getSetVoltage() << ". The output voltage was: " << channel->getOutputVoltage() << std::endl;
							__FE_SS_THROW__;
						}
					}
				}
			}
		}
		//Check that the channels are finally at the target value
		for (auto &channelGroup : activeChannelsMap_) // turns on based on the sequence determined on the config table, usually first LV, then HV
		{
			//std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << "ChannelInfo: " << channelGroup.second.size() << std::endl;
			for (auto &channelInfo : channelGroup.second)
			{
				PowerSupplyChannel *channel = channelInfo.second.channel;
				//std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << "Channel: " << channel->getID() << " Type: " << channelInfo.second.type << std::endl;
				if (!channelInfo.second.inUse || channelInfo.second.type.compare(hvTypeName) != 0)
					continue;

				int maxNumberOfExceptions = 10;
				while(maxNumberOfExceptions > 0)
				{
					timer = timeout;
					try
					{
						// Checking that the channels are within +-2V tolerance
						float tolerance = 2;
						while ( ((channel->getOutputVoltage() < channel->getSetVoltage() - tolerance)  || (channel->getOutputVoltage() > channel->getSetVoltage() + tolerance)) && timer > 0) // 5% tolerance...
						{
							//std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << "ID: " << channel->getID() << " is on? " << channel->isOn() << " voltage: " << channel->getOutputVoltage() << std::endl;
							std::this_thread::sleep_for(std::chrono::seconds(checkTimeout));
							timer -= checkTimeout;
						}
						if (timer < 0)
						{
							std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << "Channel: " << channel->getID() << " Voltage: " << voltage << " Timeout: " << timer << " seconds." << std::endl;
							__FE_SS__ << "Error: Channel " << channel->getID() << " cannot reach the set voltage " << voltage << " after " << timeout << " seconds. Turning the channel OFF. The status of the channel before turning it OFF was: " << (channel->isOn() ? "ON" : "OFF") << ". The set voltage was: " << channel->getSetVoltage() << ". The output voltage was: " << channel->getOutputVoltage() << __E__;
							__FE_SS_THROW__;
						}
						break;//SUCCESS
					}
					catch (std::exception &e)
					{
						//std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << "Channel: " << channel->getID() << " Voltage: " << voltage << " maxNumberOfExceptions: " << maxNumberOfExceptions << " Exception: " << e.what() << std::endl;
						if(maxNumberOfExceptions > 0)
						{
							--maxNumberOfExceptions;
						}
						else
						{
							//After maxNumberOfExceptions attempts, 
							//If the voltage cannot be reached it is likely that the module went in compliance and must be be kept off
							try
							{
								turnOffChannel(channel);
								channelInfo.second.inUse = false;
							}
							catch (std::exception &e)
							{
								// do nothing and throw the turn off exception
							}
							//TO BE FIXED
							/// Here I need to check all the conditions about the channel since it might go in OverCurrent or OverVoltage and if so take a decision on what do do!
							// if(e.what() != EXCEPTION DUE TO COMPLIANCE)
							// {
								std::stringstream exceptionsStream;

								exceptionsStream << "Error: Can NOT set voltage: " << voltage << " for channel: " << channel->getID() << " after " << timer << " seconds. " 
								<< "The channel has been turned OFF. The status of the channel before turning it OFF was: " << (channel->isOn() ? "ON" : "OFF") << ". " 
								<< "The set voltage was: " << channel->getSetVoltage() << ". The output voltage was: " << channel->getOutputVoltage() << ". "
								<< "The exception was: " << e.what() << "."
								<< __E__;
								std::cout << exceptionsStream.str() << std::endl;
								__FE_SS__ << exceptionsStream.str();
								__FE_SS_THROW__;
							// }
						}
					}
				}					
			}
		}

		//After reaching the temperature waiting as long as has been decided in the configuration settings
		std::this_thread::sleep_for(std::chrono::seconds(pWaitSecondsBeforeReading));

		//Finally now do the measurement
		for (auto &channelGroup : activeChannelsMap_) // turns on based on the sequence determined on the config table, usually first LV, then HV
		{
			//std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << "ChannelInfo: " << channelGroup.second.size() << std::endl;
			for (auto &channelInfo : channelGroup.second)
			{
				PowerSupplyChannel *channel = channelInfo.second.channel;
				//std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << "Channel: " << channel->getID() << " Type: " << channelInfo.second.type << std::endl;
				if (!channelInfo.second.inUse || channelInfo.second.type.compare(hvTypeName) != 0)
					continue;

				//If the channel had some problem, due to overcurrent and ..., 
				//it was turned off at the previous step, so here I am doing IVs only on the remaining channels
				if (isChannelOn(channel))
				{
					int maxNumberOfExceptions = 10;
					while(maxNumberOfExceptions > 0)
					{
						timer = timeout;
						try
						{
							double current = 0;
							for (uint reading = 0; reading < pNumberOfReadings; reading++)
							{
								//std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << "Channel: " << channel->getID() << " Voltage: " << voltage << " Reading: " << channelInfo.second.theIVCurve[voltage] << std::endl;
								current += channel->getCurrent();
								std::this_thread::sleep_for(std::chrono::milliseconds(100));
							}
							current /= pNumberOfReadings;
							
							//std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << "Channel: " << channel->getID() << " Voltage: " << voltage << " Current: " << current << std::endl;
							ivCurvePowerSupplyData.setChannelStatus(channelGroup.first, channel->getID(), current, voltage, true, time(NULL));
							break;//SUCCESS
						}
						catch (std::exception &e)
						{
							//std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << "Channel: " << channel->getID() << " Voltage: " << voltage << " maxNumberOfExceptions: " << maxNumberOfExceptions << " Exception: " << e.what() << std::endl;
							if(maxNumberOfExceptions > 0)
							{
								--maxNumberOfExceptions;
								continue;
							}
							try
							{
								turnOffChannel(channel);
								channelInfo.second.inUse = false;
							}
							catch (std::exception &e)
							{
								// do nothing and throw the turn on exception
							}
							//TO BE FIXED
							/// Here I need to check all the conditions about the channel since it might go in OverCurrent or OverVoltage and if so take a decision on what do do!
							// if(e.what() != EXCEPTION DUE TO COMPLIANCE)
							// {
								std::stringstream exceptionsStream;

								exceptionsStream << "Error: Can NOT set voltage: " << voltage << " for channel: " << channel->getID() << " after " << timer << " seconds. " 
								<< "The channel has been turned OFF. The status of the channel before turning it OFF was: " << (channel->isOn() ? "ON" : "OFF") << ". " 
								<< "The set voltage was: " << channel->getSetVoltage() << ". The output voltage was: " << channel->getOutputVoltage() << ". "
								<< "The exception was: " << e.what() << "."
								<< __E__;
								std::cout << exceptionsStream.str() << std::endl;
								__FE_SS__ << exceptionsStream.str();
								__FE_SS_THROW__;
							// }
						}
					}					
				}
			}
		}
		//std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << "Broadcasting IV Curve!" << std::endl;
		dataPublisher_.broadcastPacket(ivCurvePowerSupplyData.convertToJson(PowerSupplyData::IVCurveDataName)); // broadcast over tcp and goes to a consumer.
	}

	dataPublisher_.broadcastPacket(PowerSupplyData::EndIVCurve);

	//__FE_COUT__ << "Setting High Voltage to default ones!!!!" << __E__;
	for (auto &channelGroup : activeChannelsMap_) // turns on based on the sequence determined on the config table, usually first LV, then HV
	{
		for (auto &channelInfo : channelGroup.second)
		{
			PowerSupplyChannel *channel = channelInfo.second.channel;
			//std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << "Channel: " << channel->getID() << " Type: " << channelInfo.second.type << std::endl;
			if (channelInfo.second.inUse && channelInfo.second.type.compare(hvTypeName) == 0)
			{
				int maxNumberOfExceptions = 10;
				while(maxNumberOfExceptions > 0)
				{
					timer = timeout;
					try
					{
						//std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << "Channel: " << channel->getID() << " SettingVoltage: " << voltage << std::endl;
						channel->setVoltage(channelInfo.second.setVoltage);
						//Turn the channel back on in case it was shut down due to overcurrent or so
						if(!channel->isOn())
						{
							turnOnChannel(channel);
						}
						break;//SUCCESS
					}
					catch (std::exception &e)
					{
						//std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << "Channel: " << channel->getID() << " Voltage: " << voltage << " maxNumberOfExceptions: " << maxNumberOfExceptions << " Exception: " << e.what() << std::endl;
						if(maxNumberOfExceptions > 0)
						{
							--maxNumberOfExceptions;
							continue;
						}
						try
						{
							turnOffChannel(channel);
							channelInfo.second.inUse = false;
						}
						catch (std::exception &e)
						{
							// do nothing and throw the turn on exception
						}
						__FE_SS__ << "Error: Can NOT set voltage: " << channelInfo.second.setVoltage << " for channel: " << channel->getID() << " after " << timer << " seconds. Turning the channel OFF. The status of the channel before turning it OFF was: " << (channel->isOn() ? "ON" : "OFF") << ". The set voltage was: " << channel->getSetVoltage() << ". The output voltage was: " << channel->getOutputVoltage() << __E__;
						std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << "Error: Can NOT set voltage: " << channelInfo.second.setVoltage << " for channel: " << channel->getID() << " after " << timer << " seconds. Turning the channel OFF. The status of the channel before turning it OFF was: " << (channel->isOn() ? "ON" : "OFF") << ". The set voltage was: " << channel->getSetVoltage() << ". The output voltage was: " << channel->getOutputVoltage() << std::endl;
						__FE_SS_THROW__;
					}
				}					
			}
		}
	}
	
	for (auto &channelGroup : activeChannelsMap_) // turns on based on the sequence determined on the config table, usually first LV, then HV
	{
		for (auto &channelInfo : channelGroup.second)
		{
			PowerSupplyChannel *channel = channelInfo.second.channel;
			//std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << "Channel: " << channel->getID() << " Type: " << channelInfo.second.type << std::endl;
			if (channelInfo.second.inUse && channelInfo.second.type.compare(hvTypeName) == 0)
			{
				int maxNumberOfExceptions = 10;
				while(maxNumberOfExceptions > 0)
				{
					timer = timeout;
					try
					{
						// Checking that the channel is within +-2V tolerance
						float tolerance = 2;
						while ( ((channel->getOutputVoltage() < channel->getSetVoltage() - tolerance)  || (channel->getOutputVoltage() > channel->getSetVoltage() + tolerance)) && timer > 0) // 5% tolerance...
						{
							//std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << "ID: " << channel->getID() << " is on? " << channel->isOn() << " voltage: " << channel->getOutputVoltage() << std::endl;
							std::this_thread::sleep_for(std::chrono::seconds(checkTimeout));
							timer -= checkTimeout;
						}
						if (timer < 0)
						{
							std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << "Channel: " << channel->getID() << " Voltage: " << channelInfo.second.setVoltage << " Timeout: " << timer << " seconds." << std::endl;
							__FE_SS__ << "Error: Channel " << channel->getID() << " cannot reach the set voltage " << channelInfo.second.setVoltage << " after " << timeout << " seconds. Turning the channel OFF. The status of the channel before turning it OFF was: " << (channel->isOn() ? "ON" : "OFF") << ". The set voltage was: " << channel->getSetVoltage() << ". The output voltage was: " << channel->getOutputVoltage() << __E__;
							__FE_SS_THROW__;
						}
						break;
					}
					catch (std::exception &e)
					{
						//std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << "Channel: " << channel->getID() << " Voltage: " << voltage << " maxNumberOfExceptions: " << maxNumberOfExceptions << " Exception: " << e.what() << std::endl;
						if(maxNumberOfExceptions > 0)
						{
							--maxNumberOfExceptions;
							continue;
						}
						try
						{
							turnOffChannel(channel);
							channelInfo.second.inUse = false;
						}
						catch (std::exception &e)
						{
							// do nothing and throw the turn on exception
						}
						__FE_SS__ << "Error: Can NOT set voltage: " << channelInfo.second.setVoltage << " for channel: " << channel->getID() << " after " << timer << " seconds. Turning the channel OFF. The status of the channel before turning it OFF was: " << (channel->isOn() ? "ON" : "OFF") << ". The set voltage was: " << channel->getSetVoltage() << ". The output voltage was: " << channel->getOutputVoltage() << __E__;
						std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << "Error: Can NOT set voltage: " << channelInfo.second.setVoltage << " for channel: " << channel->getID() << " after " << timer << " seconds. Turning the channel OFF. The status of the channel before turning it OFF was: " << (channel->isOn() ? "ON" : "OFF") << ". The set voltage was: " << channel->getSetVoltage() << ". The output voltage was: " << channel->getOutputVoltage() << std::endl;
						__FE_SS_THROW__;
					}
				}					
			}
		}
	}
	std::cout << "[" << __LINE__ << "]" << "void FEPowerSupplyInterface::doIVCurves(__ARGS__)" << "Done Macro DoIVCurve!!!!" << __E__;
}

//========================================================================================================================
void FEPowerSupplyInterface::powerCycle(__ARGS__)
{
	std::cout << "[" << __LINE__ << "]" << "void FEPowerSupplyInterface::powerCycle(__ARGS__)" << "Running powercycle macro!!!!" << __E__;
	try
	{
		turnOff();
	}
	catch (const std::exception &e)
	{
		std::vector<std::string> badChannels = getListOfOffChannels();
		if (badChannels.size() == 0)
		{
			__FE_SS__ << e.what() << __E__;
			std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << e.what() << std::endl;
			__FE_SS_THROW__;
		}
		else
		{
			__FE_SS__ << "Can NOT turn off channel: ";
			for (auto channel : badChannels)
				ss << channel << "\n";
			ss << __E__;
			std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << ss.str() << std::endl;
			__FE_SS_THROW__;
		}
	}
	
	std::this_thread::sleep_for(std::chrono::milliseconds(2000));
	
	try
	{
		turnOn();
	}
	catch (const std::exception &e)
	{
		std::vector<std::string> badChannels = getListOfOffChannels();
		if (badChannels.size() == 0)
		{
			__FE_SS__ << e.what() << __E__;
			std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << e.what() << std::endl;
			__FE_SS_THROW__;
		}
		else
		{
			__FE_SS__ << "Can NOT turn on channel: ";
			for (auto channel : badChannels)
				ss << channel << "\n";
			ss << __E__;
			std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << ss.str() << std::endl;
			__FE_SS_THROW__;
		}
	}
	std::cout << "[" << __LINE__ << "]" << "void FEPowerSupplyInterface::powerCycle(__ARGS__)" << "Done Running powercycle macro!!!!" << __E__;
}

//========================================================================================================================
void FEPowerSupplyInterface::printTime(const std::string& message)
{
    auto now = std::chrono::system_clock::now();
    std::time_t current_time = std::chrono::system_clock::to_time_t(now);
    std::tm local_time = *std::localtime(&current_time);

    // Custom format: YYYY-MM-DD HH:MM:SS
    std::cout << "Current time: " 
              << std::put_time(&local_time, "%Y-%m-%d %H:%M:%S") 
			  << ".\t" << message
              << std::endl;
}


DEFINE_OTS_INTERFACE(FEPowerSupplyInterface)