#ifndef _ots_FEWRCBCInterface_h_
#define _ots_FEWRCBCInterface_h_

#include "otsdaq/FECore/FEVInterface.h"
#include "otsdaq/NetworkUtilities/UDPDataStreamerBase.h"
#include "otsdaq-cmsoutertracker/Ph2_ACF/System/SystemController.h"

#include <string>
#include <fstream>

namespace ots
{

class FECBCInterface : public FEVInterface, public UDPDataStreamerBase
{
public:

public:
	FECBCInterface(const std::string& interfaceUID, const ConfigurationTree& theXDAQContextConfigTree, const std::string& configurationPath);
	virtual ~FECBCInterface(void);

	void configure        (void);
	void halt             (void);
	void pause            (void);
	void resume           (void);
	void start            (std::string runNumber) override;
	void stop             (void);

	bool running          (void);

	void universalRead	  (char* address, char* readValue)  override  {;}
	void universalWrite	  (char* address, char* writeValue) override {;}

	//THESE 2 DON'T NEED TO BE IMPLEMENTED!!!
	//void configureFEW     (void);
	//void configureDetector(const DACStream& theDACStream) {;}
	////////////////////////////////////////////////////////////



private:
	std::string                  cHWFile_;
	bool                         saveDataFile_;
	std::string                  dataFilePath_;
	std::string                  dataFileNamePrefix_;
	Ph2_System::SystemController cSystemController;
	std::ofstream                outFile_;
	unsigned int                 eventCounter_;
	std::vector<uint32_t>        cData_;

};



}

#endif
