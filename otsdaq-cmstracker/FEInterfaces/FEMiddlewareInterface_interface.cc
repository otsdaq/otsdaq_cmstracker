#include "otsdaq/MessageFacility/MessageFacility.h"
#include "otsdaq/Macros/CoutMacros.h"
#include "otsdaq/Macros/InterfacePluginMacros.h"
#include "otsdaq-cmstracker/FEInterfaces/FEMiddlewareInterface.h"
#include "otsdaq/XmlUtilities/HttpXmlDocument.h"

#include "otsdaq-cmstracker/Ph2_ACF/Utils/easylogging++.h"
#include "otsdaq-cmstracker/Ph2_ACF/Utils/ConfigureInfo.h"
#include "otsdaq-cmstracker/Ph2_ACF/Utils/StartInfo.h"
#include "otsdaq-cmstracker/Ph2_ACF/MessageUtils/cpp/QueryMessage.pb.h"
#include "otsdaq-cmstracker/Ph2_ACF/MessageUtils/cpp/ReplyMessage.pb.h"

#include <iostream>
#include <fstream>
#include <thread>
#include <chrono>

#include <boost/process.hpp>

using namespace ots;
INITIALIZE_EASYLOGGINGPP

//========================================================================================================================
FEMiddlewareInterface::FEMiddlewareInterface(const std::string &interfaceUID, const ConfigurationTree &theXDAQContextConfigTree, const std::string &configurationPath)
	: FEVInterface(interfaceUID, theXDAQContextConfigTree, configurationPath), TCPClient(
																				   theXDAQContextConfigTree_.getNode(configurationPath).getNode("InterfaceIPAddress").getValue<std::string>(),
																				   theXDAQContextConfigTree_.getNode(configurationPath).getNode("InterfacePort").getValue<unsigned int>()),
	  calibrationName_(theXDAQContextConfigTree_.getNode(configurationPath).getNode("CalibrationName").getValue<std::string>()),
	  configurationDir_(theXDAQContextConfigTree_.getNode(configurationPath).getNode("ConfigurationDir").getValue<std::string>()),
	  configurationName_(theXDAQContextConfigTree_.getNode(configurationPath).getNode("ConfigurationName").getValue<std::string>()),
	  configurationFilePath_(configurationDir_ + "/" + configurationName_),
	  moduleConfigurationFileName_(""),
	  iterationNumber_(-1),
	  runningTimeout_(57600) // 16 hours
{
	// configure the logger
    el::Configurations conf(std::string(getenv("PH2ACF_BASE_DIR")) + "/settings/logger.conf");
    el::Loggers::reconfigureAllLoggers(conf);

	FEVInterface::registerFEMacroFunction(
		"Calibration", // feMacroName
		static_cast<FEVInterface::frontEndMacroFunction_t>(
			&FEMiddlewareInterface::calibration), // feMacroFunction
		std::vector<std::string>{"Calibration"},  // calibrationandpedenoise"},             // namesOfInputArgs
		std::vector<std::string>{},				  // namesOfOutputArgs
		1);										  // requiredUserPermissions

	FEVInterface::registerFEMacroFunction(
		"WaitForRunToCompleteAndStop", // feMacroName
		static_cast<FEVInterface::frontEndMacroFunction_t>(
			&FEMiddlewareInterface::waitForRunToCompleteAndStop), // feMacroFunction
		std::vector<std::string>{},								  // namesOfInputArgs
		std::vector<std::string>{},								  // namesOfOutputArgs
		1);														  // requiredUserPermissions

} // end constructor

//========================================================================================================================
FEMiddlewareInterface::~FEMiddlewareInterface(void)
{
} // end destructor

//========================================================================================================================
void FEMiddlewareInterface::configure(void)
{
	__FE_COUT__ << "Configure" << std::endl;
	bool firmwareLoad;
	std::string firmwareName;
	std::string readoutFrequency;
	std::string calibrationName = calibrationName_;

	try
	{
		TCPClient::connect(30, 1000); // Try for 30 seconds and then give up
	}
	catch (const std::exception &e)
	{
		__FE_SS__ << e.what() << __E__;
		__FE_SS_THROW__;
	}

	ConfigureInfo theConfigureInfo;
	try
	{
		moduleConfigurationFileName_ = __ENV__("MODULE_NAMES_FILE");
		binariesDir_ = __ENV__("OTSDAQ_CMSTRACKER_BIN");
	}
	catch (std::runtime_error &e)
	{
		// If there is no environment, proceed without.
		// In fact MODULE_NAME_FILE must only be declared if the burnin is in use, and there the environment is checked if it exists
		__FE_COUT__ << "The following is a WARNING not an error. "
					<< "It just means that there is likely no file and it is not necessary to read it. "
					   "The file is necessary when running the burnin box but this might not be the case. ->"
					<< e.what() << __E__;
		moduleConfigurationFileName_ = "";
	}

	if (moduleConfigurationFileName_ == "")
	{
		theConfigureInfo.setConfigurationFiles(configurationFilePath_);
	}
	else
	{
		HttpXmlDocument cfgXml;
		if (cfgXml.loadXmlDocument(moduleConfigurationFileName_))
		{
			std::vector<std::string> moduleLocation;
			std::vector<std::string> moduleId;
			std::vector<std::string> moduleName;
			cfgXml.getAllMatchingValues("ModuleLocation", moduleLocation);
			cfgXml.getAllMatchingValues("ModuleId", moduleId);
			cfgXml.getAllMatchingValues("ModuleName", moduleName);
			for (unsigned i = 0; i < moduleName.size(); i++)
			{
				// std::cout << __LINE__ << "] " << __PRETTY_FUNCTION__ << moduleLocation[i] << " : " << atoi(moduleId[i].c_str()) << " : " << moduleName[i] << std::endl;
				if (moduleName[i] != "Empty")
					theConfigureInfo.enableOpticalGroup(0, atoi(moduleId[i].c_str()), moduleName[i]);
			}
			configurationFilePath_ = StringMacros::convertEnvironmentVariables(cfgXml.getMatchingValue("ConfigurationFile", 0).c_str());
			firmwareLoad = cfgXml.getMatchingValue("FirmwareLoad", 0) == "Yes" ? true : false;
			firmwareName = cfgXml.getMatchingValue("Firmware", 0);
			readoutFrequency = cfgXml.getMatchingValue("ReadoutFrequency", 0);
			// std::cout << __LINE__ << "] " << __PRETTY_FUNCTION__ << "\t" << configurationFilePath_ << std::endl;
			// std::cout << __LINE__ << "] " << __PRETTY_FUNCTION__ << "\t" << readoutFrequency << std::endl;
			// std::cout << __LINE__ << "] " << __PRETTY_FUNCTION__ << "\t" << modulesType_ << std::endl;
			// If there is the configuration file then we use the xml defined in the Burninbox GUI
			theConfigureInfo.setConfigurationFiles(configurationFilePath_);
			if (calibrationName_.compare("ecv") == 0 ||
				calibrationName_.compare("fullTest") == 0 ||
				calibrationName_.compare("fullTestPart1") == 0 ||
				calibrationName_.compare("fullTestPart2") == 0 ||
				calibrationName_.compare("quickTest") == 0 ||
				calibrationName_.compare("skeletonTest") == 0)
			{
				calibrationName = cfgXml.getMatchingValue("ModulesType", 0) + calibrationName_;
			}
			if (firmwareLoad)
			{
				// Loading the firmware
				boost::process::ipstream out_stream; // For capturing the output
				boost::process::ipstream err_stream;

				// std::string loadFirmwareCommand = "/home/uplegger/Programming/otsdaq-devel/otsdaq-cmstracker/spack-build-6phjxsd/bin/fpgaconfig -c " + configurationFilePath_ + " -i " + firmwareName;
				std::string loadFirmwareCommand = binariesDir_ + "/fpgaconfig -c " + configurationFilePath_ + " -i " + firmwareName;
				// std::cout << __LINE__ << "] " << __PRETTY_FUNCTION__ << "\t" << loadFirmwareCommand << std::endl;

				try
				{
					boost::process::child c(loadFirmwareCommand, boost::process::std_out > out_stream, boost::process::std_err > err_stream);

					std::string line;
					std::string stdOut = "";
					std::string stdErr = "";
					while (out_stream && std::getline(out_stream, line) && !line.empty())
					{
						stdOut += line + "\n";
						// std::cout << "STDOUT: " << stdOut << std::endl;
					}

					// Read from stderr
					while (err_stream && std::getline(err_stream, line) && !line.empty())
					{
						stdErr += line;
						// std::cout << __LINE__ << "] STDERR: " << stdErr << std::endl;
					}

					c.wait(); // Wait for the process to exit
					std::getline(out_stream, line);

					// std::cout << __LINE__ << "]" << line << std::endl;

					if (c.exit_code() != 0)
					{
						throw std::runtime_error(stdErr);
					}
				}
				catch (const std::exception &e)
				{
					__FE_SS__ << "Error: Can't laod the FC7 firmware:\n"
							  << e.what() << __E__;
					// std::cout << __LINE__ << "]" << ss.str() << std::endl;
					__FE_SS_THROW__;
				}
			}
		}
		else
		{
			__FE_SS__ << "Error: Failed to properly load file " + moduleConfigurationFileName_ + ". Make sure it exists and it well formatted. Try to write it again." << __E__;
			__FE_SS_THROW__;
		}
	}

	theConfigureInfo.setCalibrationName(calibrationName);

	std::string theCommandString = theConfigureInfo.createProtobufMessage();
	const uint maxAttempts = 5;
	bool configuredSuccessfully = false;
	std::string lastUnsuccessfulMessage = "";
	for (uint attempt = 0; attempt < maxAttempts; attempt++)
	{
		try
		{
			configuredSuccessfully = false;
			checkReturnMessage(sendCommand(theCommandString));
			configuredSuccessfully = true;
			break;
		}
		catch (const std::runtime_error &e)
		{
			if (std::string(e.what()).find("No object enabled in fDetectorContainer") != std::string::npos)
			{
				lastUnsuccessfulMessage = e.what();
				halt();
				disconnect();
				try
				{
					TCPClient::connect(30, 1000); // Try for 30 seconds and then give up
				}
				catch (const std::exception &e)
				{
					__FE_SS__ << e.what() << __E__;
					__FE_SS_THROW__;
				}
			}
		}
	}
	if (!configuredSuccessfully)
	{
		__FE_SS__ << "After " << maxAttempts << " unsuccessful attempts to configure the detector we are giving up!"
				  << " The last failed attempt message from the Ph2_ACF was: " << lastUnsuccessfulMessage
				  << " Suggestion: Is the power to the modules on? " << __E__;
		__FE_SS_THROW__;
	}

	// std::string readBuffer = TCPClient::sendAndReceivePacket("Configure,Calibration:" + calibrationName_ + ",ConfigurationFile:" + configurationFilePath);
	//__FE_COUT__ << "Message received: " << readBuffer << std::endl;
}

//========================================================================================================================
void FEMiddlewareInterface::halt(void)
{
	iterationNumber_ = -1;
	__FE_COUT__ << std::endl;
	MessageUtils::QueryMessage theQuery;
	theQuery.mutable_query_type()->set_type(MessageUtils::QueryType::HALT);
	std::string theCommandString;
	theQuery.SerializeToString(&theCommandString);

	checkReturnMessage(sendCommand(theCommandString));
	resetPacketNumber();
}

//========================================================================================================================
void FEMiddlewareInterface::pause(void)
{
	__FE_COUT__ << std::endl;
	__FE_COUT__ << "Not sending anything for PAUSE!" << std::endl;
	// std::string readBuffer;
	// readBuffer = TCPClient::sendAndReceivePacket("Pause");
	// __FE_COUT__ << "Message received: " << readBuffer << std::endl;
}

//========================================================================================================================
void FEMiddlewareInterface::resume(void)
{
	__FE_COUT__ << std::endl;
	__FE_COUT__ << "Not sending anything for RESUME!" << std::endl;
	// std::string readBuffer;
	// readBuffer = TCPClient::sendAndReceivePacket("Resume");
	// __FE_COUT__ << "Message received: " << readBuffer << std::endl;
}

//========================================================================================================================
void FEMiddlewareInterface::start(std::string runNumber)
{
	__FE_COUT__ << "Starting..." << std::endl;
	doneStartStop_ = false;
	runNumber_ = runNumber;
	std::string iterationValue;
	if (iterationNumber_ < 0)
		iterationValue = "start";
	else
		iterationValue = std::to_string(iterationNumber_);
	++iterationNumber_;
	std::string iterationString = "Iteration_" + iterationValue;

	StartInfo theStartInfo;
	theStartInfo.setRunNumber(stoi(runNumber_));
	theStartInfo.setAppendInformation(iterationString);

	std::string theCommandString = theStartInfo.createProtobufMessage();

	std::thread t(&FEMiddlewareInterface::startStopTimer, this);
	try
	{
		checkReturnMessage(sendCommand(theCommandString));
	}
	catch (const std::exception &e)
	{
		doneStartStop_ = true;
		t.join();
		__FE_SS__ << e.what() << __E__;
		std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << e.what() << std::endl;
		__FE_SS_THROW__;
	}

	// TCPClient::sendAndReceivePacket("Start:{RunNumber:" + runNumber + "}");
	doneStartStop_ = true;
	t.join();
	startTime_ = std::chrono::steady_clock::now();
	__FE_COUT__ << "Done Starting..." << std::endl;
}

//========================================================================================================================
void FEMiddlewareInterface::startIteration(std::string runNumber, std::string append)
{
	__FE_COUT__ << "Starting Iteration..." << std::endl;
	// TCPClient::sendAndReceivePacket("Iteration:{RunNumber:" + runNumber + ",Append:" + append + "}");
	__FE_COUT__ << "Done Starting Iteration..." << std::endl;
}

//========================================================================================================================
// The running state is a thread
bool FEMiddlewareInterface::running(void)
{
	//__FE_COUT__ << "running " << std::endl;
	// std::string readBuffer;
	// readBuffer = TCPClient::sendAndReceivePacket("Status?");

	printTime("FEMiddlewareInterface::running() Workloop: " + std::to_string(WorkLoop::continueWorkLoop_));
	MessageUtils::QueryMessage theQuery;
	theQuery.mutable_query_type()->set_type(MessageUtils::QueryType::STATUS);
	std::string theCommandString;
	theQuery.SerializeToString(&theCommandString);
	std::string readBuffer = sendCommand(theCommandString);
	__FE_COUT__ << "Message received: " << "[" << __LINE__ << "] " << readBuffer << std::endl;

	MessageUtils::ReplyMessage theStatus;
	theStatus.ParseFromString(readBuffer);

	switch (theStatus.reply_type().type())
	{
	case MessageUtils::ReplyType::RUNNING:
	{
		std::chrono::steady_clock::time_point currentTime = std::chrono::steady_clock::now();
		std::chrono::steady_clock::duration elapsedTime = currentTime - startTime_;

		// Calculate elapsed time in hours and minutes
		auto elapsedSecondsTotal = std::chrono::duration_cast<std::chrono::seconds>(elapsedTime);
		int elapsedHours = elapsedSecondsTotal.count() / 3600;
		int elapsedMinutes = (elapsedSecondsTotal.count() % 3600) / 60;
		int elapsedSeconds = (elapsedSecondsTotal.count() % 60);

		// Calculate remaining time in hours and minutes
		auto remainingTime = runningTimeout_ - elapsedTime;
		auto remainingSecondsTotal = std::chrono::duration_cast<std::chrono::seconds>(remainingTime);
		int remainingHours = remainingSecondsTotal.count() / 3600;
		int remainingMinutes = (remainingSecondsTotal.count() % 3600) / 60;
		int remainingSeconds = (remainingSecondsTotal.count() % 60);

		// Print elapsed and remaining time
		std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__
				  << "\tElapsed Time: in running state " << elapsedHours << " hours, " << elapsedMinutes << " minutes, " << elapsedSeconds << " seconds. "
				  << "Remaining Time: " << remainingHours << " hours, " << remainingMinutes << " minutes, " << remainingSeconds << " seconds. "
				  << std::endl;
		if (elapsedTime >= runningTimeout_)
		{
			std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << "\tSomething went wrong with the Ph2_ACF, still in running state after " << runningTimeout_.count() << " seconds. I will stop running." << std::endl;
			WorkLoop::continueWorkLoop_ = false;
		}
		break;
	}
	case MessageUtils::ReplyType::SUCCESS:
	{
		printTime("FEMiddlewareInterface::running() Success!");
		WorkLoop::continueWorkLoop_ = false;
		break;
	}
	case MessageUtils::ReplyType::ERROR:
	{
		WorkLoop::continueWorkLoop_ = false;
		__FE_COUT__ << "I got an Error from RunController: " << theStatus.message();
		std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << "\tI got an Error from RunController: " << theStatus.message() << std::endl;
		__SS__ << "ERROR RunController exception: " << theStatus.message() << std::endl;
		__SS_THROW__;
		break;
	}
	default:
	{
		__FE_COUT__ << "Invalid reply received: " << readBuffer << __E__;
	}
	}
	std::this_thread::sleep_for(std::chrono::seconds(5));
	// WorkLoop::continueWorkLoop_ = false;
	return WorkLoop::continueWorkLoop_; // otherwise it stops!!!!!
}

//========================================================================================================================
void FEMiddlewareInterface::stop(void)
{
	__FE_COUT__ << std::endl;
	MessageUtils::QueryMessage theQuery;
	theQuery.mutable_query_type()->set_type(MessageUtils::QueryType::STOP);
	std::string theCommandString;
	theQuery.SerializeToString(&theCommandString);
	std::string readBuffer = sendCommand(theCommandString);

	// std::string readBuffer;
	// readBuffer = TCPClient::sendAndReceivePacket("Stop");
	startTime_ = std::chrono::steady_clock::now(); // Unnecessary but...
	//__FE_COUT__ << "Message received: " << readBuffer << std::endl;
}

//========================================================================================================================
void FEMiddlewareInterface::startStopTimer(void)
{
	const int timeout = 30;
	const int exceptionTimeout = 100;
	const int checkTimeout = 1; // seconds
	int timer = 0;
	unsigned int iterationIndex = VStateMachine::getIterationIndex();

	while (!doneStartStop_)
	{
		std::this_thread::sleep_for(std::chrono::seconds(checkTimeout));
		timer += checkTimeout;
		if (timer > timeout)
		{
			if (iterationIndex > exceptionTimeout)
			{
				// Not sure an exception is the right thing in a thread
				//  __FE_SS__ << "Error: I have spent already " << timeout*exceptionTimeout << " seconds waiting in this state." << std::endl;
				//  __FE_SS_THROW__;
				std::cout << "Error: I have spent already " << timeout * exceptionTimeout << " seconds waiting in this state." << std::endl;
				break;
			}
			timer = 0;
			VStateMachine::indicateIterationWork();
		}
	}
	std::cout << __LINE__ << "]" << __PRETTY_FUNCTION__ << "Timer done" << std::endl;
}

//========================================================================================================================
void FEMiddlewareInterface::calibration(__ARGS__)
{
    printTime("Begin FEMiddlewareInterface::calibration");
	// std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << "\t# of input args = " << argsIn.size() << __E__;
	// std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << "\t# of output args = " << argsOut.size() << __E__;
	// for (auto &argIn : argsIn)
	// 	std::cout << __PRETTY_FUNCTION__ << argIn.first << ": " << argIn.second << __E__;

	std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << "\tWorkloop status: " << WorkLoop::continueWorkLoop_ << __E__;
	while (WorkLoop::continueWorkLoop_ == true)
	{
		std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << "\tStill running from previous command. Workloop: " << WorkLoop::continueWorkLoop_ << __E__;
		std::this_thread::sleep_for(std::chrono::milliseconds(500));
	}

	calibrationName_ = __GET_ARG_IN__("Calibration", std::string);

	std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << "\tDoing " << calibrationName_ << __E__;

	// used to halt and reconfigure -> Issue with the visualizers, the server is restarted but the clients do not reconnect
	halt();
	disconnect();
	configure();

	// std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << "Starting Macro. Workloop: " << WorkLoop::continueWorkLoop_ << __E__;
	// std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << "Starting Macro. Workloop: " << WorkLoop::continueWorkLoop_ << __E__;
	// std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << "Starting Macro. Workloop: " << WorkLoop::continueWorkLoop_ << __E__;
	std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << "\tStarting Macro. Workloop: " << WorkLoop::continueWorkLoop_ << __E__;

	start(runNumber_);
	WorkLoop::continueWorkLoop_ = true;
	while (WorkLoop::continueWorkLoop_ == true)
	{
		//std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << "\tStill running...Macro. Workloop: " << WorkLoop::continueWorkLoop_ << __E__;
	  	printTime("FEMiddlewareInterface::calibration() Workloop: " + std::to_string(WorkLoop::continueWorkLoop_));
		running();
		// std::this_thread::sleep_for(std::chrono::milliseconds(500));
	}
	stop();
	// std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << "\tDone with macro. Workloop: " << WorkLoop::continueWorkLoop_ << __E__;
	// std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << "\tDone with macro. Workloop: " << WorkLoop::continueWorkLoop_ << __E__;
	std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << "\tDone with macro. Workloop: " << WorkLoop::continueWorkLoop_ << __E__;
    printTime("End   FEMiddlewareInterface::calibration");

} // end calibration()

//========================================================================================================================
void FEMiddlewareInterface::waitForRunToCompleteAndStop(__ARGS__)
{
    printTime("Begin FEMiddlewareInterface::waitForRunToCompleteAndStop");
	std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << "\tWorkloop status: " << WorkLoop::continueWorkLoop_ << __E__;
	while (WorkLoop::continueWorkLoop_ == true)
	{
		//std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << "\tStill running from previous command. Workloop: " << WorkLoop::continueWorkLoop_ << __E__;
		std::this_thread::sleep_for(std::chrono::milliseconds(500));
	}
	// while (WorkLoop::continueWorkLoop_ == true)
	// {
	// 	// std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << "\tStill running...Macro. Workloop: " << WorkLoop::continueWorkLoop_ << __E__;
	// 	// std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << "\tStill running...Macro. Workloop: " << WorkLoop::continueWorkLoop_ << __E__;
	// 	// std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << "\tStill running...Macro. Workloop: " << WorkLoop::continueWorkLoop_ << __E__;
	// 	std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << "\tStill running...Macro. Workloop: " << WorkLoop::continueWorkLoop_ << __E__;
	// 	running();
	// 	std::this_thread::sleep_for(std::chrono::milliseconds(500));
	// }
	stop();
	// std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << "Done with macro. Workloop: " << WorkLoop::continueWorkLoop_ << __E__;
	// std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << "Done with macro. Workloop: " << WorkLoop::continueWorkLoop_ << __E__;
	std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << "\tDone with macro. Workloop: " << WorkLoop::continueWorkLoop_ << __E__;
    printTime("End   FEMiddlewareInterface::waitForRunToCompleteAndStop");
}

//========================================================================================================================
std::string FEMiddlewareInterface::sendCommand(const std::string &command)
{
	std::lock_guard<std::mutex> lock(networkMutex_);
	try
	{
		return TCPClient::sendAndReceivePacket(command);
	}
	catch (const std::exception &e)
	{
		throw(std::runtime_error(e.what()));
		return "";
	}
}

//========================================================================================================================
void FEMiddlewareInterface::checkReturnMessage(const std::string &message)
{
	MessageUtils::ReplyMessage theReturnedMessage;
	theReturnedMessage.ParseFromString(message);
	if (theReturnedMessage.reply_type().type() == MessageUtils::ReplyType::ERROR)
	{
		__FE_COUT__ << "ERROR: Message received: " << message << std::endl;
		std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ <<"\tERROR: Message received: " << message << std::endl;
		throw(std::runtime_error(theReturnedMessage.message()));
	}
	else if (theReturnedMessage.reply_type().type() == MessageUtils::ReplyType::SUCCESS)
		__FE_COUT__ << "SUCCESS: Message received: " << message << std::endl;
	else if (theReturnedMessage.reply_type().type() == MessageUtils::ReplyType::RUNNING)
		__FE_COUT__ << "RUNNING: Message received: " << message << std::endl;
	else
		__FE_COUT__ << "Message received with reply type number: " << message << std::endl;
}

//========================================================================================================================
void FEMiddlewareInterface::printTime(const std::string& message)
{
    auto now = std::chrono::system_clock::now();
    std::time_t current_time = std::chrono::system_clock::to_time_t(now);
    std::tm local_time = *std::localtime(&current_time);

    // Custom format: YYYY-MM-DD HH:MM:SS
    std::cout << "Current time: " 
              << std::put_time(&local_time, "%Y-%m-%d %H:%M:%S") 
			  << ".\t" << message
              << std::endl;
}

DEFINE_OTS_INTERFACE(FEMiddlewareInterface)
