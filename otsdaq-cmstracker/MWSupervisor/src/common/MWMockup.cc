#include "MWMockup/MWMockup.h"

#include "xoap/MessageFactory.h"
#include "xoap/Method.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPBody.h"
#include "xoap/domutils.h"

#include "log4cplus/layout.h"

using namespace Ph2TkDAQ;

XDAQ_INSTANTIATOR_IMPL(MWMockup)

MWMockup::MWMockup(xdaq::ApplicationStub* s) throw (xdaq::exception::Exception) :
  xdaq::Application(s),
  xgi::framework::UIManager(this)
{
  std::string  pattern = "%d{%y/%m/%d %H:%M:%S} [%c{2}] <%M:%L> $> %m %n";
  std::auto_ptr<log4cplus::Layout>  _layout(new  log4cplus::PatternLayout(pattern));
  this->getApplicationLogger().getAppender("console")->setLayout(_layout);

  LOG4CPLUS_INFO(this->getApplicationLogger(), "This is a mockup of middleware.");

  xgi::framework::deferredbind(this, this, &MWMockup::Default, "Default");
  xoap::bind(this, &MWMockup::sayHello, "hello", XDAQ_NS_URI );

  std::stringstream ss;
  ss << this->getApplicationLogger().getName() << " " << this->getApplicationLogger().getAllAppenders()[0]->getName();
  LOG4CPLUS_INFO(this->getApplicationLogger(), ss.str());
}


void
MWMockup::Default(xgi::Input* in,
                  xgi::Output* out)
  throw (xgi::exception::Exception)
{
  *out << cgicc::HTMLDoctype(cgicc::HTMLDoctype::eStrict) << std::endl;
  *out << cgicc::html().set("lang", "en").set("dir","ltr") << std::endl;
  *out << cgicc::title("Middleware mockup") << std::endl;
  *out << cgicc::h1("Middleware mockup") << std::endl;
  *out << cgicc::p("This is the page for mockup middleware.") << std::endl;
}


xoap::MessageReference
MWMockup::sayHello( xoap::MessageReference msg )
  throw (xoap::exception::Exception)
{
  xoap::SOAPPart part = msg->getSOAPPart();
  xoap::SOAPEnvelope env = part.getEnvelope();
  xoap::SOAPBody body = env.getBody();
  DOMNode* node = body.getDOMNode();
  DOMNodeList* bodyList = node->getChildNodes();

  DOMNode* command = nullptr;
  std::string commandName;

  for (unsigned int i(0); i<bodyList->getLength(); ++i)
  {
    command = bodyList->item(i);
    if (command->getNodeType() == DOMNode::ELEMENT_NODE)
    {
      commandName = xoap::XMLCh2String( command->getLocalName() );
      break;
    }
  }

  if (commandName.empty())
    XDAQ_LOG_AND_RAISE( xoap::exception::Exception, this->getApplicationLogger(), "Command not found.");
  else
    LOG4CPLUS_INFO( this->getApplicationLogger(), "Receving command - "+commandName);

  std::string replyMessage = "Hello back!";

  xoap::MessageReference reply = xoap::createMessage();
  xoap::SOAPEnvelope envelope = reply->getSOAPPart().getEnvelope();
  xoap::SOAPName responseName = envelope.createName( commandName+"Response", "xdaq", XDAQ_NS_URI );
  xoap::SOAPBodyElement e = envelope.getBody().addBodyElement ( responseName );

  xoap::SOAPName contentTag = envelope.createName( "content", "xdaq", XDAQ_NS_URI );
  xoap::SOAPElement contentElem = e.addChildElement( contentTag );
  xoap::SOAPName rmsgTag = envelope.createName( "message", "xdaq", XDAQ_NS_URI);
  contentElem.addAttribute( rmsgTag, replyMessage );

  return reply;
}