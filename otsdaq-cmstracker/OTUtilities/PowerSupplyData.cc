#include "otsdaq-cmstracker/OTUtilities/PowerSupplyData.h"
#include <iostream>
#include <iomanip>
#include <sstream>
#include <nlohmann/json.hpp>

using json = nlohmann::json;

using namespace ots;

const std::string PowerSupplyData::StatusDataName  = "StatusData";
const std::string PowerSupplyData::IVCurveDataName = "IVCurveData";
const std::string PowerSupplyData::BeginIVCurve    = "BeginIVCurve";
const std::string PowerSupplyData::EndIVCurve      = "EndIVCurve";

//========================================================================================================================
PowerSupplyData::PowerSupplyData()
{
}

//========================================================================================================================
PowerSupplyData::~PowerSupplyData()
{
}

//========================================================================================================================
void PowerSupplyData::addChannel(std::string group, std::string channel)
{
    configuredChannelsMap_[group][channel] = ChannelStatus();
}

//========================================================================================================================
void PowerSupplyData::setChannelStatus(std::string group, std::string channel, float current, float voltage, bool status, time_t time)
{
    configuredChannelsMap_[group][channel].current = current;
    configuredChannelsMap_[group][channel].voltage = voltage;
    configuredChannelsMap_[group][channel].status = status;
    configuredChannelsMap_[group][channel].time = time;
}

//========================================================================================================================
PowerSupplyData::ChannelStatus PowerSupplyData::getChannelStatus(std::string group, std::string channel)
{
    return configuredChannelsMap_[group][channel];
}

//========================================================================================================================
std::string PowerSupplyData::convertToJson(std::string dataName)
{
    if(dataName != PowerSupplyData::StatusDataName  && dataName != PowerSupplyData::IVCurveDataName)
    {
        std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << " This cannot happen so I am aborting. It just means that the code is wrong!" << std::endl;
        exit(0);
    }
    
    json j;
    for (auto &group : configuredChannelsMap_)
    {
        for (auto &channel : group.second)
        {
            j[dataName][group.first][channel.first]["A"] = channel.second.current;
            j[dataName][group.first][channel.first]["V"] = channel.second.voltage;
            j[dataName][group.first][channel.first]["S"] = channel.second.status;
            j[dataName][group.first][channel.first]["T"] = channel.second.time;
        }
    }
    return j.dump();
}

//========================================================================================================================
std::map<std::string, std::map<std::string, std::map<std::string, PowerSupplyData::ChannelStatus>>> PowerSupplyData::convertFromJson(std::string jsonin)
{
    json j = json::parse(jsonin);
    std::map<std::string, std::map<std::string, std::map<std::string, PowerSupplyData::ChannelStatus>>> returnMap;
    for (auto &dataName : j.items())
    {
        for (auto &group : dataName.value().items())
        {
            for (auto &channel : group.value().items())
            {
                //std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << dataName.key() << " " <<  group.key() << " " << channel.key() << std::endl;
                returnMap[dataName.key()][group.key()][channel.key()].current = j[dataName.key()][group.key()][channel.key()]["A"];
                returnMap[dataName.key()][group.key()][channel.key()].voltage = j[dataName.key()][group.key()][channel.key()]["V"];
                returnMap[dataName.key()][group.key()][channel.key()].status  = j[dataName.key()][group.key()][channel.key()]["S"];
                returnMap[dataName.key()][group.key()][channel.key()].time    = j[dataName.key()][group.key()][channel.key()]["T"];
            }
        }
    }
    return returnMap;
}