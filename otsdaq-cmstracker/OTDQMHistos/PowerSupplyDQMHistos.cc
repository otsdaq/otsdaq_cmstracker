#include "otsdaq-cmstracker/OTDQMHistos/PowerSupplyDQMHistos.h"
#include "otsdaq/ConfigurationInterface/ConfigurationTree.h"
#include "otsdaq/Macros/CoutMacros.h"
#include "otsdaq/MessageFacility/MessageFacility.h"
#include "otsdaq/XmlUtilities/HttpXmlDocument.h"

#include <iostream>
#include <sstream>
#include <string>
#include <iomanip>
#include <ctime>


#include <TCanvas.h>
#include <TDirectory.h>
#include <TFile.h>
#include <TFrame.h>
#include <TGraph.h>
#include <TH1.h>
#include <TH1F.h>
#include <TH2.h>
#include <TH2F.h>
#include <TTree.h>
#include <TKey.h>
#include <TMultiGraph.h>
#include <TProfile.h>
#include <TROOT.h>
#include <TRandom.h>
#include <TStyle.h>
#include <TThread.h>

using namespace ots;

#define DEFAULT_MARKER_STYLE 20
#define DEFAULT_MARKER_SIZE 0.4
// #define TIME_FORMAT "%H:%M:%S %m-%d-%Y"
#define TIME_FORMAT "%Y-%m-%d %H:%M:%S"
#define TIME_FORMAT_FOR_DIRECTORY "%Y-%m-%d_%H-%M-%S"

//========================================================================================================================
PowerSupplyDQMHistos::PowerSupplyDQMHistos(bool deleteHistos)
	: cAllLowVoltages_(nullptr), cAllHighVoltages_(nullptr), mgAllLVCurrents_(nullptr), mgAllHVCurrents_(nullptr), mgAllLVVoltages_(nullptr), mgAllHVVoltages_(nullptr), deleteHistos_(deleteHistos)
{
	gStyle->SetOptTitle(0);
	gStyle->SetTitleFillColor(2);
	gStyle->SetTitleFontSize(0.2);
	gStyle->SetTimeOffset(0);
}

//========================================================================================================================
PowerSupplyDQMHistos::~PowerSupplyDQMHistos(void)
{
	if (deleteHistos_)
		destroy();
}

//========================================================================================================================
void PowerSupplyDQMHistos::book(TDirectory *myDirectory, const ConfigurationTree &theXDAQContextConfigTree, const std::string &theConfigurationPath)
{
	// clearPointers();
	__COUT__ << "Booking start!" << std::endl;

	std::vector<std::string> burninModules;
	std::string moduleConfigurationFileName;
	try
	{
		moduleConfigurationFileName = __ENV__("MODULE_NAMES_FILE");
	}
	catch (std::runtime_error &e)
	{
		// If there is no environment, proceed without.
		// In fact MODULE_NAME_FILE must only be declared if the burnin is in use, and there the environment is checked if it exists
		__COUT__ << "The following is a WARNING not an error. "
				 << "It just means that there is likely no file and it is not necessary to read it. "
					"The file is necessary when running the burnin box but this might not be the case. ->"
				 << e.what() << __E__;
		moduleConfigurationFileName = "";
	}

	if (moduleConfigurationFileName != "")
	{
		HttpXmlDocument cfgXml;
		if (cfgXml.loadXmlDocument(moduleConfigurationFileName))
		{
			std::vector<std::string> moduleLocation;
			std::vector<std::string> moduleId;
			std::vector<std::string> moduleName;
			cfgXml.getAllMatchingValues("ModuleLocation", moduleLocation);
			cfgXml.getAllMatchingValues("ModuleId", moduleId);
			cfgXml.getAllMatchingValues("ModuleName", moduleName);

			for (uint i = 0; i < moduleName.size(); i++)
			{
				if (moduleName[i] != "Empty") // Module location must be found at the beginning of the name
				{
					burninModules.push_back(moduleLocation[i]);
				}
			}
		}
		else
		{
			__SS__ << "Error: Failed to properly use file " + moduleConfigurationFileName + ". Make sure it exists and it well formatted. Try to write it again." << __E__;
			__SS_THROW__;
		}
	}

	myDir_ = myDirectory;
	statusDir_ = myDir_->mkdir("Status", "Status"); // creates folder for these plots
	ivCurveDir_ = myDir_->mkdir("IVCurves", "IVCurves");

	// Old way
	// Loop over the rows of PowerSupplyToDetectorTable
	uint colorCounter = 0;
	for (auto &channel : theXDAQContextConfigTree.getNode(theConfigurationPath + "/LinkToPowerSupplyToDetectorTable").getChildren())
	{
		std::string channelName = theXDAQContextConfigTree.getNode(theConfigurationPath + "/LinkToPowerSupplyToDetectorTable/" + channel.second.getValueAsString() + "/LinkToChannelUID").getValue();
		std::string channelType = theXDAQContextConfigTree.getNode(theConfigurationPath + "/LinkToPowerSupplyToDetectorTable/" + channel.second.getValueAsString() + "/ChannelType").getValue();
		std::string channelInUse = theXDAQContextConfigTree.getNode(theConfigurationPath + "/LinkToPowerSupplyToDetectorTable/" + channel.second.getValueAsString() + "/LinkToChannelTable/InUse").getValue();
		uint histoColor = theXDAQContextConfigTree.getNode(theConfigurationPath + "/LinkToPowerSupplyToDetectorTable/" + channel.second.getValueAsString() + "/HistoColor").getValue<unsigned>();
		// std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << "Channel: " << channelName << " Type: " << channelType << " InUse: " << channelInUse << __E__;
		if (channelInUse.compare("No") == 0 || channelInUse.compare("no") == 0)
			continue;

		TCanvas *aCanvas = nullptr;
		TGraph *aGraph = nullptr;
		if (channelType == "HV")
		{
			std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << "IVCurve Channel: " << channelName << " Type: " << channelType << " InUse: " << channelInUse << __E__;
			bool found = false;
			for (auto moduleLocation : burninModules)
			{
				if (channelName.find(moduleLocation) == 0) // Module location must be found at the beginning of the name)
				{
					found = true;
					break;
				}
			}
			if (found && (ivCurveMap_.find(channelName) == ivCurveMap_.end() || (aCanvas = ivCurveMap_.find(channelName)->second.canvasIVCurve) == nullptr))
			{
				ivCurveDir_->cd();
				aCanvas = ivCurveMap_[channelName].canvasIVCurve = new TCanvas((channelName + "_IVCurve").c_str(), (channelName + " IV Curve").c_str(), 800, 600);
				ivCurveDir_->Add(aCanvas);
				aCanvas->SetGridx();
				aCanvas->SetGridy();
				aGraph = ivCurveMap_[channelName].graphIVCurve = new TGraph();
				aGraph->SetNameTitle(("g" + channelName + "IVCurve").c_str(), (channelName + " IV Curve").c_str());
				aGraph->GetXaxis()->SetTitle("Voltage [V]");
				aGraph->GetYaxis()->SetTitle("Current [uA]");
				aGraph->SetMarkerColor(histoColor);
				aGraph->SetLineColor(histoColor);
				aGraph->SetMarkerStyle(DEFAULT_MARKER_STYLE);
				aGraph->SetMarkerSize(DEFAULT_MARKER_SIZE);
				// aGraph->GetXaxis()->SetTimeDisplay(1);
				// aGraph->GetXaxis()->SetNdivisions(503);
				// aGraph->GetXaxis()->SetTimeFormat(TIME_FORMAT);

				aCanvas->cd();
				aGraph->Draw("APL");

				aCanvas = ivCurveMap_[channelName].canvasIVCurveTimestamp = new TCanvas((channelName + "_IVCurveTimestamp").c_str(), (channelName + " IV Curve Timestamp").c_str(), 800, 600);
				ivCurveDir_->Add(aCanvas);
				aCanvas->SetGridx();
				aCanvas->SetGridy();
				aGraph = ivCurveMap_[channelName].graphIVCurveTimestamp = new TGraph();
				aGraph->SetNameTitle(("g" + channelName + "IVCurveTimestamp").c_str(), (channelName + " IV Curve Timestamp").c_str());
				aGraph->GetXaxis()->SetTitle("Voltage [V]");
				aGraph->GetYaxis()->SetTitle("Local Time");
				aGraph->SetMarkerColor(histoColor);
				aGraph->SetLineColor(histoColor);
				aGraph->SetMarkerStyle(DEFAULT_MARKER_STYLE);
				aGraph->SetMarkerSize(DEFAULT_MARKER_SIZE);
				aGraph->GetYaxis()->SetTimeDisplay(1);
				aGraph->GetYaxis()->SetNdivisions(503);
				aGraph->GetYaxis()->SetTimeFormat(TIME_FORMAT);

				aCanvas->cd();
				aGraph->Draw("APL");
			}
		}

		statusDir_->cd();
		if (canvasMap_.find(channelName) == canvasMap_.end() || (aCanvas = canvasMap_.find(channelName)->second) == nullptr)
		{
			aCanvas = canvasMap_[channelName] = new TCanvas(channelName.c_str(), channelName.c_str(), 800, 600);
			aCanvas->Divide(1, 2);
			statusDir_->Add(aCanvas);
		}
		// aCanvas here MUST point to an existing canvas.
		// If aCanvas = nullptr it is created
		// otherwise it is assigned in the second part of the or in the if statement

		if (graphMap_.find(channelName + "_Voltage") == graphMap_.end())
		{
			aGraph = graphMap_[channelName + "_Voltage"] = new TGraph();
			aGraph->SetNameTitle((channelName + "_Voltage").c_str(), (channelName + " Voltage").c_str());
			aGraph->GetXaxis()->SetTitle("Local Time");
			aGraph->GetYaxis()->SetTitle("Voltage [V]");
			aGraph->SetMarkerColor(histoColor);
			aGraph->SetLineColor(histoColor);
			aGraph->SetMarkerStyle(DEFAULT_MARKER_STYLE);
			aGraph->SetMarkerSize(DEFAULT_MARKER_SIZE);
			aGraph->GetXaxis()->SetTimeDisplay(1);
			aGraph->GetXaxis()->SetNdivisions(503);
			aGraph->GetXaxis()->SetTimeFormat(TIME_FORMAT);

			aCanvas->cd(1);
			gPad->SetGridx();
			gPad->SetGridy();
			aGraph->Draw("APL");
		}
		else
		{
			graphMap_.find(channelName + "_Voltage")->second->Clear();
		}

		aGraph = nullptr;
		if (graphMap_.find(channelName + "_Current") == graphMap_.end())
		{
			aGraph = graphMap_[channelName + "_Current"] = new TGraph();
			aGraph->SetNameTitle((channelName + "_Current").c_str(), (channelName + " Current").c_str());
			aGraph->GetXaxis()->SetTitle("Local Time");
			if (channelType == "HV")
			{
				aGraph->GetYaxis()->SetTitle("Current [uA]");
			}
			else
			{
				aGraph->GetYaxis()->SetTitle("Current [A]");
			}
			aGraph->SetMarkerColor(histoColor);
			aGraph->SetLineColor(histoColor);
			aGraph->SetMarkerStyle(DEFAULT_MARKER_STYLE);
			aGraph->SetMarkerSize(DEFAULT_MARKER_SIZE);
			aGraph->GetXaxis()->SetTimeDisplay(1);
			aGraph->GetXaxis()->SetNdivisions(503);
			aGraph->GetXaxis()->SetTimeFormat(TIME_FORMAT);

			aCanvas->cd(2);
			gPad->SetGridx();
			gPad->SetGridy();
			aGraph->Draw("APL");
		}
		else
		{
			graphMap_.find(channelName + "_Current")->second->Clear();
		}

		if (channelType == "LV")
		{
			// std::cout << "entered lv" << std::endl;
			lowVoltageVoltageMap[channelName + channelType] = graphMap_[channelName + "_Voltage"];
			lowVoltageCurrentMap[channelName + channelType] = graphMap_[channelName + "_Current"];
		}
		else if (channelType == "HV")
		{
			// std::cout << "entered hv" << std::endl;
			highVoltageVoltageMap[channelName + channelType] = graphMap_[channelName + "_Voltage"];
			highVoltageCurrentMap[channelName + channelType] = graphMap_[channelName + "_Current"];
		}
		else
		{
			__SS__ << "ChannelType column for channel " << channelName << " must be configured to be LV or HV and cannot be DEFAULT" << __E__;
			__SS_THROW__;
		}
		if (colorCounter > 9)
			colorCounter = 0;
		else
			++colorCounter;
	}

	if (cAllLowVoltages_ == nullptr)
	{
		cAllLowVoltages_ = new TCanvas("All_LV", "All Low Voltages", 800, 600); // creation of multigraphs for currents and voltages
		statusDir_->Add(cAllLowVoltages_);
		cAllLowVoltages_->Divide(1, 2);

		mgAllLVVoltages_ = new TMultiGraph();
		mgAllLVVoltages_->GetXaxis()->SetTitle("Local Time");
		mgAllLVVoltages_->GetYaxis()->SetTitle("Voltage [V]");
		mgAllLVVoltages_->SetNameTitle("mgAllLVVoltages", "All Low Voltages");
		mgAllLVVoltages_->GetXaxis()->SetTimeDisplay(1);
		mgAllLVVoltages_->GetXaxis()->SetNdivisions(503);
		mgAllLVVoltages_->GetXaxis()->SetTimeFormat(TIME_FORMAT);
		for (auto &listLowVoltageVoltages : lowVoltageVoltageMap)
		{
			mgAllLVVoltages_->Add(listLowVoltageVoltages.second);
		}
		cAllLowVoltages_->cd(1);
		gPad->SetGridx();
		gPad->SetGridy();
		mgAllLVVoltages_->Draw("APL");

		mgAllLVCurrents_ = new TMultiGraph();
		mgAllLVCurrents_->SetNameTitle("mgAllLVCurrents", "All Low Voltage Currents");
		mgAllLVCurrents_->GetXaxis()->SetTitle("Local Time");
		mgAllLVCurrents_->GetYaxis()->SetTitle("Current [A]");
		mgAllLVCurrents_->GetXaxis()->SetTimeDisplay(1);
		mgAllLVCurrents_->GetXaxis()->SetNdivisions(503);
		mgAllLVCurrents_->GetXaxis()->SetTimeFormat(TIME_FORMAT);
		for (auto &listLowVoltageCurrents : lowVoltageCurrentMap)
		{
			mgAllLVCurrents_->Add(listLowVoltageCurrents.second);
		}
		cAllLowVoltages_->cd(2);
		gPad->SetGridx();
		gPad->SetGridy();
		mgAllLVCurrents_->Draw("APL");
	}

	if (cAllHighVoltages_ == nullptr)
	{
		cAllHighVoltages_ = new TCanvas("All_HV", "All High Voltages", 800, 600);
		statusDir_->Add(cAllHighVoltages_);
		cAllHighVoltages_->Divide(1, 2);

		mgAllHVVoltages_ = new TMultiGraph();
		mgAllHVVoltages_->SetNameTitle("mgAllHVVoltages", "All High Voltages");
		mgAllHVVoltages_->GetXaxis()->SetTitle("Local Time");
		mgAllHVVoltages_->GetYaxis()->SetTitle("Voltage [V]");
		mgAllHVVoltages_->GetXaxis()->SetTimeDisplay(1);
		mgAllHVVoltages_->GetXaxis()->SetNdivisions(503);
		mgAllHVVoltages_->GetXaxis()->SetTimeFormat(TIME_FORMAT);

		for (auto &listHighVoltageVoltages : highVoltageVoltageMap)
		{
			mgAllHVVoltages_->Add(listHighVoltageVoltages.second);
		}
		cAllHighVoltages_->cd(1);
		gPad->SetGridx();
		gPad->SetGridy();
		mgAllHVVoltages_->Draw("APL");

		mgAllHVCurrents_ = new TMultiGraph();
		mgAllHVCurrents_->SetNameTitle("mgAllHVCurrents", "All High Voltage Currents");
		mgAllHVCurrents_->GetXaxis()->SetTitle("Local Time");
		mgAllHVCurrents_->GetYaxis()->SetTitle("Current [uA]");
		mgAllHVCurrents_->GetXaxis()->SetTimeDisplay(1);
		mgAllHVCurrents_->GetXaxis()->SetNdivisions(503);
		mgAllHVCurrents_->GetXaxis()->SetTimeFormat(TIME_FORMAT);

		for (auto &listHighVoltageCurrents : highVoltageCurrentMap)
		{
			mgAllHVCurrents_->Add(listHighVoltageCurrents.second);
		}

		cAllHighVoltages_->cd(2);
		gPad->SetGridx();
		gPad->SetGridy();
		mgAllHVCurrents_->Draw("APL");
	}

	__COUT__ << "Booking done!" << std::endl;
}

//========================================================================================================================
void PowerSupplyDQMHistos::fill(std::string &buffer)
{
	//std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << "Filling histos!" << std::endl;
	try
	{
		//std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << "Buffer:-" << buffer << std::endl;
		if (buffer.compare(PowerSupplyData::BeginIVCurve) == 0)
		{
			//std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << "Begin:-" << buffer << std::endl;
			time_t now = time(nullptr);
			struct tm tstruct;
			char buf[80];
			tstruct = *localtime(&now);
			strftime(buf, sizeof(buf), TIME_FORMAT_FOR_DIRECTORY, &tstruct);
			beginIVCurveTime_ = buf;
			//std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << "BeginTime:-" << beginIVCurveTime_ << std::endl;

			ResetDir(ivCurveDir_);
		}
		else if (buffer.compare(PowerSupplyData::EndIVCurve) == 0)
		{
			//std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << "End:-" << buffer << std::endl;
 			
			std::string dirName = "IVCurves_" + beginIVCurveTime_;

			TDirectory *destinationDir = myDir_->mkdir(dirName.c_str());
			destinationDir->cd();
			CopyDir(ivCurveDir_);
		}
		else
		{
			//std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << "Else:-" << buffer << std::endl;
			// get the information from buffer and convert it from Json format
			auto jsonMap = thePowerSupplyData_.convertFromJson(buffer);
			const std::string dataName = jsonMap.begin()->first;
			std::map<std::string, std::map<std::string, PowerSupplyData::ChannelStatus>> channelInfoMap = jsonMap[dataName];
			std::string channelName;

			for (auto &group : channelInfoMap)
			{
				for (auto &channel : group.second)
				{
					// fill graphs with values from buffer
					channelName = channel.first;
					//std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << "Channel name: " << channelName << std::endl;
					if (dataName == PowerSupplyData::StatusDataName)
					{
						if (channel.second.time < 1000)
							return;
						//std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << "Channel: " << channelName << " Time: " << channel.second.time << std::endl;

						TCanvas *aCanvas = canvasMap_.find(channelName)->second;
						std::map<std::string, TGraph *>::iterator aGraph;
						if ((aGraph = graphMap_.find(channelName + "_Voltage")) != graphMap_.end())
						{
							aGraph->second->SetPoint(aGraph->second->GetN(), channel.second.time, channel.second.voltage);
							aCanvas->cd(1);
							aGraph->second->Draw("APL");
						}
						if ((aGraph = graphMap_.find(channelName + "_Current")) != graphMap_.end())
						{
							aGraph->second->SetPoint(aGraph->second->GetN(), channel.second.time, channel.second.current);
							aCanvas->cd(2);
							aGraph->second->Draw("APL");
						}
					}
					else if (dataName == PowerSupplyData::IVCurveDataName)
					{
						//std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << "IV Curve channel name: " << channelName << " V: " << channel.second.voltage << " C: " << channel.second.current << std::endl;

						if (ivCurveMap_.find(channelName) != ivCurveMap_.end())
						{
							TGraph *anIVCurveGraph = ivCurveMap_.find(channelName)->second.graphIVCurve;
							anIVCurveGraph->SetPoint(anIVCurveGraph->GetN(), channel.second.voltage, channel.second.current);
							TGraph *anIVCurveTimestampGraph = ivCurveMap_.find(channelName)->second.graphIVCurveTimestamp;
							anIVCurveTimestampGraph->SetPoint(anIVCurveTimestampGraph->GetN(), channel.second.voltage, channel.second.time);
						}
					}
					else
					{
						std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << "This cannot happen, returning!" << std::endl;
						return;
					}
				}
			}

			if (dataName == PowerSupplyData::StatusDataName)
			{
				cAllLowVoltages_->cd(1);
				mgAllLVVoltages_->Draw("APL");
				cAllLowVoltages_->cd(2);
				mgAllLVCurrents_->Draw("APL");
				cAllHighVoltages_->cd(1);
				mgAllHVVoltages_->Draw("APL");
				cAllHighVoltages_->cd(2);
				mgAllHVCurrents_->Draw("APL");
			}
		}
	}
	catch (const std::runtime_error &e)
	{
		// The status was probably not ready so just returning...
		__COUT__ << "Error: Exception filling histos!" << std::endl;
		std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << "Error: Exception filling histos!" << std::endl;
		return;
	}
	//std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << "Done Filling histos!" << std::endl;
}

//========================================================================================================================
void PowerSupplyDQMHistos::destroy()
{
	for (auto cIt = canvasMap_.begin(); cIt != canvasMap_.end(); cIt++)
	{
		if (cIt->second != nullptr)
			delete cIt->second;
	}
	canvasMap_.clear();
	for (auto gIt = graphMap_.begin(); gIt != graphMap_.end(); gIt++)
	{
		if (gIt->second != nullptr)
			delete gIt->second;
	}
	graphMap_.clear();

	if (cAllLowVoltages_ != nullptr)
		delete cAllLowVoltages_;
	if (cAllHighVoltages_ != nullptr)
		delete cAllHighVoltages_;
	if (mgAllLVCurrents_ != nullptr)
		delete mgAllLVCurrents_;
	if (mgAllHVCurrents_ != nullptr)
		delete mgAllHVCurrents_;
	if (mgAllLVVoltages_ != nullptr)
		delete mgAllLVVoltages_;
	if (mgAllHVVoltages_ != nullptr)
		delete mgAllHVVoltages_;

	clearPointers();
}

//========================================================================================================================
void PowerSupplyDQMHistos::clearPointers()
{
	canvasMap_.clear(); // Once the map is cleared the pointers are gone so no need to set them to null
	graphMap_.clear();	// Once the map is cleared the pointers are gone so no need to set them to null
	cAllLowVoltages_  = nullptr;
	cAllHighVoltages_ = nullptr;
	mgAllLVCurrents_  = nullptr;
	mgAllHVCurrents_  = nullptr;
	mgAllLVVoltages_  = nullptr;
	mgAllHVVoltages_  = nullptr;
}

//========================================================================================================================
void PowerSupplyDQMHistos::CopyDir(TDirectory* source)
{
    TDirectory* savdir = gDirectory;
    TDirectory* adir   = savdir->mkdir(source->GetName());
    adir->cd();
    TList *theListOfObjects = source->GetList();
    TList *theListOfKeys = source->GetListOfKeys();
    std::map<std::string, TObject*> theMapOfObjects; // because vector are better then TList
    for(int i=0; i<theListOfKeys->GetSize(); ++i) 
    {
        theMapOfObjects[theListOfKeys->At(i)->GetName()] = theListOfKeys->At(i);
    }
    for(int i=0; i<theListOfObjects->GetSize(); ++i)// Object should come after to save the most recent plot (TObject) and not the one saved in the file (TKey)
    {
        theMapOfObjects[theListOfObjects->At(i)->GetName()] = theListOfObjects->At(i);
    }

    for(auto theInputNameAndObject : theMapOfObjects)
    {
        const char* classname = theInputNameAndObject.second->ClassName();
        //std::cout<< __PRETTY_FUNCTION__ << " [" << __LINE__ << "] Class Name = " << classname << std::endl;
        //std::cout<< __PRETTY_FUNCTION__ << " [" << __LINE__ << "] Object Name = " << theInputNameAndObject.first << std::endl;
        // TClass* cl = gROOT->GetClass(classname);
        // if(!cl) continue;
        if(theInputNameAndObject.second->InheritsFrom("TDirectory"))
        {
            source->cd(theInputNameAndObject.first.c_str());
            TDirectory* subdir = gDirectory;
            adir->cd();
            CopyDir(subdir);
            adir->cd();
        }
        else if(theInputNameAndObject.second->InheritsFrom("TTree"))
        {
            TTree* T = (TTree*)source->Get(theInputNameAndObject.first.c_str());
            adir->cd();
            TTree* newT = T->CloneTree();
            newT->Write();
        }
        else if(theInputNameAndObject.second->InheritsFrom("TKey"))
        {
            // source->cd();
            auto theInputKey = static_cast<TKey*>(theInputNameAndObject.second);
            //std::cout << "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! Copying key " << theInputNameAndObject.first << std::endl;
            TObject* obj = theInputKey->ReadObj();
            // std::cout << theInputNameAndObject.first << " : " << adir->GetName() << std::endl;
            adir->cd();
            obj->Write(theInputNameAndObject.first.c_str());
            delete obj;
            obj = nullptr;
        }
        else
        {
            std::cout << "[" << __LINE__ << "]\tCopying object " << theInputNameAndObject.first << std::endl;
            adir->cd();
            theInputNameAndObject.second->Write();
        }
    }
    adir->SaveSelf(kTRUE);
    savdir->cd();
}

//========================================================================================================================
void PowerSupplyDQMHistos::ResetDir(TDirectory *source)
{
	TList *MyList = source->GetList();
	TIter next(MyList);
	TObject *key;
	while ((key = (TObject *)next()))
	{
		//std::cout << "Name: " << key->GetName() << std::endl;
        if (key->InheritsFrom(TDirectory::Class()))
		{
			source->cd(key->GetName());
			TDirectory *subdir = gDirectory;
			ResetDir(subdir);
		}
		else if (key->InheritsFrom(TTree::Class()))
		{
			TTree *T = (TTree *)source->Get(key->GetName());
			T->Reset();
		}
        else if (key->InheritsFrom(TGraph::Class()))
        {
            //std::cout << "Found TGraph " << std::endl;
            // static_cast<TGraph*>(key)->Set(0);
        }
        else if (key->InheritsFrom(TCanvas::Class()))
        {
            //std::cout << "Found TCanvas " << std::endl;
            // Retrieve the list of primitives (objects) from the canvas
            TList *primitives = static_cast<TCanvas*>(key)->GetListOfPrimitives();

            // Find the TGraph in the list of primitives
            TGraph *retrievedGraph = nullptr;
            TObject *obj;
            TIter next(primitives);
            while ((obj = next())) {
                if (obj->InheritsFrom(TGraph::Class())) {
                    retrievedGraph = (TGraph*)obj;
                    retrievedGraph->Set(0);
                    break;
                }
            }
        }
        else if (key->InheritsFrom(TH1::Class()))
            static_cast<TH1 *>(key)->Reset();
		else
		{
			std::cout << "Don't know which object you are talking about: " << key->GetName() << std::endl;
		}
	}
}
