# Otsdaq CMS Tracker

## OTSDAQ Core Installation
This code is an extension of the otsdaq project: https://github.com/art-daq/otsdaq/wiki project and requires it to function. 
The following instruction must be executed once and they are needed to install OTSDAQ.
Once the OTSDAQ core dependencies are set up, the CMS Tracker interface code can be included.  

# ALMA 9.5
Most of the packages needed to run OTSDAQ and the Ph2_ACF are hosted in the cern repo.
It is recommended to install the ALMA 9 CERN version.
To install the CERN ALMA 9 version outside CERN you should follow these instructions.

1. Download a generic Alma9 ISO, e.g. the DVD ISO linked from https://almalinux.org/get-almalinux/#ISO_Images.
2. Boot from that ISO
3. Add http://linuxsoft.cern.ch/cern/alma/9.5/CERN/x86_64/ as additional repository
4. Select the option to install the "CERN Workstation"

If you have another ALMA 9 version you need to install the cern repositories.
https://linux.web.cern.ch/updates/alma9/

# OTSDAQ INSTALLATION ON ALMA 9
<!--
### The manual installation instructions are collected into the following [Install.sh](https://gitlab.cern.ch/otsdaq/otsdaq_cmstracker/-/blob/develop/SetupFiles/Install.sh?ref_type=heads) script that you can just download and run!
### WARNING: The script will create the 'otsdaq' directory from the location where it is run. 
### You need sudo powers and need to install [protobuf](https://gitlab.cern.ch/cms_tk_ph2/MessageUtils/-/blob/master/README.md) yourself!
-->
## Manual installation
<!--
### If you didn't run the Install.sh script  then you can copy and paste in a terminal the following instructions
-->
### Install OTSDAQ system dependencies (need to be root or have sudo powers):

<pre>
sudo dnf install -y libdb-devel
sudo dnf install -y gdbm-devel
sudo dnf install -y libfontenc-devel
sudo dnf install -y libpciaccess libpciaccess-devel
sudo dnf install -y libtirpc-devel
sudo dnf install -y libXdmcp-devel libXxf86vm-devel Lmod mesa-libGLU mesa-libGLU-devel mesa-libGL-devel
sudo dnf install -y meson nasm ninja-build patchelf 
sudo dnf install -y python python3 python-devel python3-devel
sudo dnf install -y texinfo texlive mesa-libGL-devel
sudo dnf install -y xorg-x11-util-macros xorg-x11-server-devel xorg-x11-xtrans-devel
sudo dnf install -y libxcb libxcb-devel 
sudo dnf install -y xcb-util 
sudo dnf install -y xcb-util-wm xcb-util-wm-devel 
sudo dnf install -y xcb-util-keysyms xcb-util-keysyms-devel 
sudo dnf install -y xcb-util-image xcb-util-image-devel
sudo dnf install -y xcb-util-renderutil xcb-util-renderutil-devel
sudo dnf install -y libxkbcommon libxkbcommon-devel libxkbcommon-x11 libxkbcommon-x11-devel  
sudo dnf install -y libX11-xcb
sudo dnf install -y gcc-gfortran
sudo dnf install -y freetype freetype-devel
sudo dnf install -y binutils-devel
sudo dnf install -y patch
</pre>

## Ipbus is also needed for the communication between the Ph2_ACF and the FC7 (need to be root or have sudo powers). If you already installed a standalone version of Ph2_ACF then you can skip this step.

Follow instructions at:
https://ph2acf.docs.cern.ch/general/required_install/

## Protobuf is also needed for the communication between OTSDAQ and the Ph2_ACF (need to be root or have sudo powers). If you already installed a standalone version of Ph2_ACF then you can skip this step.

Follow instructions at:
https://gitlab.cern.ch/cms_tk_ph2/MessageUtils/-/blob/master/README.md

## CAEN HV Wrapper libraries are needed to control the CAEN power supply (need to be root or have sudo powers).
Download the tar file here after logging in:
https://www.caen.it/?downloadfile=7011

<pre>
tar zxvf CAENHVWrapper-6.3.tgz 
cd CAENHVWrapper-6.3/
./install.sh 
</pre>
 

### The following instructions will install OTSDAQ, the Tracker and Burnin packages. They can be copied in a shell script and then run from there.


- This installation will create 2 directories:

    1. otsdaq/spack (where all the packages are installed)

    2. otsdaq/user (where we keep the data, configuration database and user data for OTSDAQ)

<pre>
###################################################
#FIRST: Create and go to the directory where you want to install OTSDAQ.
#As an example, here we create the otsdaq directory and we will cd into it
###################################################
mkdir otsdaq && cd otsdaq
export OTSDAQ_HOME=$PWD #This will be the directory where YOU want to install OTSDAQ
export CACTUSROOT=/opt/cactus

###################################################
#Installing Spack Fermilab stuff
###################################################
cd ${OTSDAQ_HOME}
mkdir -p spack/repos
cd spack
git clone https://github.com/FNALssi/spack.git -b fnal-develop
cd spack
git reset --hard 5d8ac7656ad4b8c2850e76d4751b9e0333d80d6b
cd ..
echo 'export SPACK_DISABLE_LOCAL_CONFIG=true 
source spack/share/spack/setup-env.sh' > setup-env.sh
source setup-env.sh
git clone https://github.com/fnalssi/fermi-spack-tools.git
cd fermi-spack-tools
git reset --hard 06627a1b963fefcfc922a4789696f1e5adf27511
cd ..
./fermi-spack-tools/bin/make_packages_yaml spack

###################################################
#Create and activate ots environment
###################################################
cd ..
spack compiler find
spack env create ots
spack env activate ots

###################################################
#Installing art suite
###################################################
cd ${OTSDAQ_HOME}
cd spack/repos
#git clone https://github.com/FNALssi/fnal_art.git && spack repo add fnal_art
git clone -b eflumerf/DontUseMasterCMake https://github.com/eflumerf/fnal_art.git && spack repo add fnal_art
#sed -i 's/libxml2@2.9.12/libxml2@2.9.13/g' ${OTSDAQ_HOME}/spack/repos/fnal_art/packages/art-suite/package.py
#git clone https://github.com/uplegger/fnal_art.git && spack repo add fnal_art
cd fnal_art
git reset --hard a150e04757263b39dd7b01140c0e7b4a20ac92e1

cd ${OTSDAQ_HOME}
spack add art-suite@s126
spack concretize -f
spack install -j`nproc`

###################################################
#Installing otsdaq suite
###################################################
cd ${OTSDAQ_HOME}
cd spack/repos

git clone https://github.com/art-daq/artdaq-spack.git && spack repo add artdaq-spack
cd artdaq-spack
git reset --hard e7f4a8998b0497ee6831a0ddb1e7ae0a487031bb

cd ${OTSDAQ_HOME}
spack add otsdaq-suite@v2_08_00 artdaq=31207 s=126
spack concretize -f
spack install -j`nproc`
spack install -j1

###################################################
#Installing Tracker and Burninbox packages
###################################################
cd ${OTSDAQ_HOME}
cd spack/repos
git clone https://gitlab.cern.ch/otsdaq/spack/otsdaq-cms-burninbox-spack.git && spack repo add otsdaq-cms-burninbox-spack
git clone https://gitlab.cern.ch/otsdaq/spack/otsdaq-cms-tracker-spack.git && spack repo add otsdaq-cms-tracker-spack

cd ..
spack add otsdaq-cmstracker
spack add otsdaq-cmsburninbox
spack concretize -f
spack install -j`nproc`
</pre>

### OTSDAQ and the Tracker and Burnin packes should be now installed.

### Now we need to install a Burnin center specific otsdaq repository
<pre>
cd ${OTSDAQ_HOME}
git clone https://gitlab.cern.ch/otsdaq/otsdaq-cms-burninbox-data.git
</pre>

### Get the SetupFile that needs to be sourced every time a new terminal is opened
<pre>
cd ${OTSDAQ_HOME}
cp otsdaq-cms-burninbox-data/SetupFiles/BurninBoxSetup.sh .
source BurninBoxSetup.sh
</pre>


### Create the directory where data histograms are stored
Chose any directory **where you want to store YOUR data**. 
The calibration files and environment files will be stored there.
The following is an example assuming you want to store the data in ${OTSDAQ_HOME}

<pre>
cd ${OTSDAQ_HOME}
mkdir -p OtsdaqData/BurninBoxOtsdaqData/BurninBoxHistos
</pre>


### Get the RunBurninBox.sh script that will start ots and RunController and BurninBoxController
<pre>
cd ${OTSDAQ_HOME}
cp otsdaq-cms-burninbox-data/SetupFiles/RunBurninBox.sh .
chmod 744 RunBurninBox.sh
</pre>


##  READ THIS FIRST BEFORE RUNNING OTSDAQ
**The Setup file you just copied (BurninBoxSetup.sh or TrackerSetup.sh) is a generic setup file, which is very similar to the Setup file example in the next paragraph.**


**There are 5 important variables that needs to be modified in order for OTSDAQ to work in YOUR environment**
1) USER_DATA
2) ARTDAQ_DATABASE_URI
3) OTSDAQ_DATA
4) BURNINBOX_CONFIGURATION_FILE
5) MODULE_NAMES_FILE

Variables meaning:
1) USER_DATA  -> points to a directory where Logs, RunNumber and other infos are stored. Each center will have its own USER_DATA directory.
2) ARTDAQ_DATABASE_URI -> has the database with the history of all configurations
3) OTSDAQ_DATA -> is the directory where **ALL DATA WILL BE SAVED**
4) BURNINBOX_CONFIGURATION_FILE -> if you use the BurninBox, you MUST configure this file according to the way YOU wired your box
5) MODULE_NAMES_FILE -> is the file where the module names are stored when a burnin cycle starts

**My reccomendation is to copy the default configurations to your custom configurations and then modify them accordingly**

**EXAMPLE**
I like to use my institution name to store those quantities so in the following example I will use Fermilab. For the other centers try to use any of the following please:
Brussels
DESY
Louvain
NCP
Niser
Pisa
Princeton
Rutgers

##  Running OTSDAQ the VERY FIRST TIME ONLY!!
From a terminal window
<pre>
cd ${OTSDAQ_HOME}
source BurninBoxSetup.sh #Needs to be run only once when a new terminal is opened
ots -w
</pre>

The **ots -w** command transforms some environmental variables into the link they correspond to. This command must be executed ONLY the very first time after a clean installation of OTSDAQ. 

##  Running OTSDAQ
From a terminal window
<pre>
cd ${OTSDAQ_HOME}
source BurninBoxSetup.sh #Needs to be run only once when a new terminal is opened
ots
</pre>

Copy and paste in Firefox or Chrome the link that appears on the terminal when OTSDAQ has started.

##  Running OTSDAQ, RunController and BurninBoxController
If you want to run the burnin and also run calibrations you need 2 more terminals and you need to type in each of them:
<pre>
cd ${OTSDAQ_HOME}
source BurninBoxSetup.sh #Needs to be run only once when a new terminal is opened
BurninBoxController
RunController
</pre>

Alternatively you can just run the script that will run ots, RunController and BurninController opening 2 extra terminals
<pre>
cd ${OTSDAQ_HOME}
source BurninBoxSetup.sh #Needs to be run only once when a new terminal is opened
./RunBurninBox.sh
</pre>


##  Setup file example
export CONFIGURATION_NAME=Default
#export PROJECT_HOME=$(echo $PWD | sed -e 's|/user||')
export PROJECT_HOME=$PWD
export USER_DATA_HOME=${PROJECT_HOME}/otsdaq-cms-burninbox-data/${CONFIGURATION_NAME}
export SPACK_HOME=${PROJECT_HOME}/spack
export OTS_MAIN_PORT=2015

export SPACK_DISABLE_LOCAL_CONFIG=true 
source ${SPACK_HOME}/spack/share/spack/setup-env.sh

spack env activate ots

#return
export MRB_SOURCE=$PROJECT_HOME

##### DATA DIRECTORY CONFIGURATION ##########
#OTSDAQ_DATA is the dirctory where all histograms are stored!!
export OTSDAQ_DATA="${PROJECT_HOME}/OtsdaqData/BurninBoxOtsdaqData"

##### OTSDAQ MAIN ENVIRONMENT VARIABLES CONFIGURATION ##########
#USER_DATA is the directory where RunNumber, Logs and other user informations are stored
export USER_DATA="${USER_DATA_HOME}/UserData"
#ARTDAQ_DATABASE_URI is the variable pointing to the USER database
export ARTDAQ_DATABASE_URI="filesystemdb://${USER_DATA_HOME}/Database"

##### BURNIN BOX CONFIGURATION ##########
#Each center has its own specific configuration for the sensors on the controller - NCP
export BURNINBOX_CONFIGURATION_FILE="${USER_DATA_HOME}/HardwareConfiguration/BurninBoxConfiguration_${CONFIGURATION_NAME}.xml"
export MODULE_NAMES_FILE="${USER_DATA_HOME}/tmp/ModuleNames.cfg"

##### OUTERTRACKER CONFIGURATION ##########
#export OTSDAQ_CMSTRACKER_DIR=${SPACK_HOME}/otsdaq-cmstracker
export PH2ACF_BASE_DIR=${OTSDAQ_CMSTRACKER_DIR}/otsdaq-cmstracker/Ph2_ACF
export ACFSUPERVISOR_ROOT=${OTSDAQ_CMSTRACKER_DIR}/otsdaq-cmstracker/ACFSupervisor

ln -fs ${OTSDAQ_CMSBURNINBOX_DIR}/UserWebGUI ${OTSDAQ_UTILITIES_DIR}/WebGUI/CMSBurninBoxWebPath

echo -e "setup [275]  \t Now your user data path is USER_DATA \t\t = ${USER_DATA}"
echo -e "setup [275]  \t Now your database path is ARTDAQ_DATABASE_URI \t = ${ARTDAQ_DATABASE_URI}"
echo -e "setup [275]  \t Now your output data path is OTSDAQ_DATA \t = ${OTSDAQ_DATA}"
echo
echo
echo -e "setup [275]  \t Now use 'ots --wiz' to configure otsdaq"
echo -e "setup [275]  \t Then use 'ots' to start otsdaq"
echo -e "setup [275]  \t Or use 'ots --help' for more options"
echo
echo -e "setup [275]  \t Use 'kx' to kill otsdaq processes"
echo

alias kx='ots -k'







# CENTOS 7 INSTALLATION (Support for CENTOS will disappear soon)

##  Install OTSDAQ system dependencies (need to be root or have sudo powers):
<pre>
sudo yum install -y git libuuid-devel openssl-devel curl-devel #as root for basic tools
sudo yum install -y gcc kernel-devel make #as root for compiling
sudo yum install -y lsb #as root for lsb_release
sudo yum install -y kde-baseapps #as root for kdialog
sudo yum install -y python2-devel elfutils-devel 
sudo yum install -y epel-release #repository to find libzstd and xxhash
sudo yum install -y libzstd xxhash xxhash-devel
sudo yum install -y mesa-lib*
sudo yum install -y pugixml pugixml-devel
sudo yum install -y xmlrpc-c-devel
sudo yum install -y boost-devel pugixml-devel json-devel
</pre>

## Set up your products area:
<pre>
sudo yum install -y https://ecsft.cern.ch/dist/cvmfs/cvmfs-release/cvmfs-release-latest.noarch.rpm
sudo yum clean all
sudo yum install -y cvmfs cvmfs-config-default
</pre>

create the file /etc/cvmfs/default.d/70-artdaq.conf and add to it the following lines:

CVMFS_REPOSITORIES=fermilab.opensciencegrid.org

CVMFS_HTTP_PROXY=DIRECT

<pre>
echo -e "CVMFS_REPOSITORIES=fermilab.opensciencegrid.org\nCVMFS_HTTP_PROXY=DIRECT" > /etc/cvmfs/default.d/70-artdaq.conf
</pre>

Then you can refresh the cvmfs configuration:

<pre>
sudo cvmfs_config setup
# Check if CernVM-FS mounts the specified repositories by: 
sudo cvmfs_config probe
</pre>
If the probe succed you should see this line

`Probing /cvmfs/fermilab.opensciencegrid.org... OK`

If the probe fails, try to restart autofs with 

`sudo service autofs restart`

If this one fails too, then check the documentation of the CERN cvmfs system here:

https://cvmfs.readthedocs.io/en/stable/cpt-quickstart.html

**From now on you don't need sudo powers anymore and it is reccommended to install the software as a regular user**


## Install OTSDAQ in user area 

If cvmfs is installed correctly, now you can export the PRODUCTS directory:
<pre>
export PRODUCTS=/cvmfs/fermilab.opensciencegrid.org/products/artdaq/
</pre>

## Fetch code from the repository

<pre>
cd $HOME
mkdir otsdaq
cd otsdaq
export OTSDAQ_HOME=$PWD
bash
source $PRODUCTS/setup # e.g. /data/ups/setup, Products set in previous step
Base=$PWD
setup git
setup gitflow
setup mrb
export MRB_PROJECT=otsdaq
mrb newDev -f -q s126:e28:prof

. $Base/local*/setup
cd $MRB_SOURCE
</pre>

Get the main software repositories
<pre>
mrb gitCheckout -b 96f772220ff12baf98d50cc829de89e6394a3c29 https://github.com/art-daq/otsdaq.git
mrb gitCheckout -b 17c5f8258eb514468e0060551a19f0f792fa5b65 https://github.com/art-daq/otsdaq_utilities.git
</pre>

OTSDAQ is now installed but **NOT** compiled!
You should continue with the installation of the Tracker package and the BurninBox package (https://gitlab.cern.ch/otsdaq/otsdaq_cmsburninbox) if needed.
You can compile anytime but do it after installing all packages saves you a bit of time.

## Installation of the Tracker package
<pre>
cd $MRB_SOURCE # this is the 'srcs' directory that will be set in the course of setting up OTSDAQ
git clone -b master --recurse-submodules https://gitlab.cern.ch/otsdaq/otsdaq_cmstracker.git otsdaq_cmstracker
mrb uc
</pre>

## Installation of the BurninBox package (If you are installing otsdaq for the BurninBox)
If you are installing otsdaq for the BurninBox system you need to install also the BurninBox package

<pre>
cd $MRB_SOURCE # this is the 'srcs' directory that will be set in the course of setting up OTSDAQ
git clone -b master https://gitlab.cern.ch/otsdaq/otsdaq_cmsburninbox.git otsdaq_cmsburninbox
mrb uc 
</pre>

There is an extra step for the BurninBox GUI that needs to point to a specific area

<pre>
cd $OTSDAQ_HOME
ln -fs $MRB_SOURCE/otsdaq_cmsburninbox/UserWebGUI $MRB_SOURCE/otsdaq_utilities/WebGUI/CMSBurninBoxWebPath
</pre>


## Copy the setup file 
**If you are installing the software for the BurninBox**

<pre>
cd $OTSDAQ_HOME
cp $MRB_SOURCE/otsdaq_cmsburninbox/SetupFiles/BurninBoxSetup.sh .
source BurninBoxSetup.sh
</pre>

**If instead you ONLY installed the tracker package, then copy the setup file for the tracker, otherwise skip this step and USE the BurninBoxSetup.sh**
<pre>
cd $OTSDAQ_HOME
cp $MRB_SOURCE/otsdaq_cmstracker/SetupFiles/TrackerSetup.sh .
source TrackerSetup.sh
</pre>

Once the package is checked out **or every time that you start a new session**, source the BurninBoxSetup.sh (or TrackerSetup.sh).

**N.B. When sourcing the BurninBoxSetup.sh (or TrackerSetup.sh) file, WARNINGS are not a problem and can be ignored.**

##  Compile
<pre>
cd $OTSDAQ_HOME
source BurninBoxSetup.sh # (or source TrackerSetup.sh)
</pre>

Right now protobuf must be compiled manually with the following commands, after the correct g++ is sourced (source BurninBoxSetup.sh or TrackerSetup.sh)
This needs to be done **only the first time** or if protobuf is upgraded.

<pre>
cd $MRB_SOURCE
mrb uc
cd $MRB_SOURCE/otsdaq_cmstracker/protobuf/protobuf_3_19_4
./autogen.sh
./configure --prefix=$MRB_SOURCE/otsdaq_cmstracker/protobuf
make -j$(nproc) # $(nproc) ensures it uses all cores for compilation
make install
cd $OTSDAQ_HOME
</pre>

Checkout Dev branch of Ph2_ACF
<pre>
cd $MRB_SOURCE/otsdaq_cmstracker/otsdaq-cmstracker/Ph2_ACF
git checkout --recurse-submodules Dev
cd $OTSDAQ_HOME
</pre>

To compile otsdaq we are using mrb and some aliases are defined in the TrackerSetup.sh 
<pre>
cd $OTSDAQ_HOME
mz #if it is the first time or when the project need to to be cleaned (use mb instead if you already started a compilation once with mz)
</pre>

**If there are no errors while compiling you have installed OTSDAQ!**

##  READ THIS FIRST BEFORE RUNNING OTSDAQ
**The Setup file you just copied (BurninBoxSetup.sh or TrackerSetup.sh) is a generic setup file, which is very similar to the Setup file example in the next paragraph.**


**There are 5 important variables that needs to be modified in order for OTSDAQ to work in YOUR environment**
1) USER_DATA
2) ARTDAQ_DATABASE_URI
3) OTSDAQ_DATA
4) BURNINBOX_CONFIGURATION_FILE
5) MODULE_NAMES_FILE

Variables meaning:
1) USER_DATA  -> points to a directory where Logs, RunNumber and other infos are stored. Each center will have its own USER_DATA directory.
2) ARTDAQ_DATABASE_URI -> has the database with the history of all configurations
3) OTSDAQ_DATA -> is the directory where **ALL DATA WILL BE SAVED**
4) BURNINBOX_CONFIGURATION_FILE -> if you use the BurninBox, you MUST configure this file according to the way YOU wired your box
5) MODULE_NAMES_FILE -> is the file where the module names are stored when a burnin cycle starts

**My reccomendation is to copy the default configurations to your custom configurations and then modify them accordingly**

**EXAMPLE**
I like to use my institution name to store those quantities so in the following example I will use Fermilab. For the other centers try to use any of the following please:
Brussels
DESY
Louvain
NCP
Niser
Pisa
Princeton
Rutgers

Create a directory where you want to store the data, **for example: $HOME/CMSBurninBox** and substitute FErmilab with your own institution name

<pre>
mkdir $HOME/CMSBurninBox
cd $HOME/CMSBurninBox
cp -rfp ${OTSDAQ_HOME}/srcs/otsdaq_cmsburninbox/DataDefault $HOME/CMSBurninBox/DataFermilab
cp -rfp ${OTSDAQ_HOME}/srcs/otsdaq_cmsburninbox/databases/filesystemdb/2023_10_Default_db $HOME/CMSBurninBox/filesystemdb/2023_10_Fermilab_db
cp -p ${OTSDAQ_HOME}/srcs/otsdaq_cmsburninbox/BurninBox/configuration/BurninBoxConfiguration_Default.xml ${OTSDAQ_HOME}/srcs/otsdaq_cmsburninbox/BurninBox/configuration/BurninBoxConfiguration_Fermilab.xml
</pre>


##  Running OTSDAQ the VERY FIRST TIME
From a terminal window
<pre>
cd $OTSDAQ_HOME/otsdaq
source BurninBoxSetup.sh # (or source TrackerSetup.sh)
ots -w
</pre>

The **ots -w** command transforms some environmental variables into the link they correspond to. This command must be executed ONLY the very first time after a clean installation of OTSDAQ. 

##  Running OTSDAQ
From a terminal window
<pre>
cd $OTSDAQ_HOME/otsdaq
source BurninBoxSetup.sh # (or source TrackerSetup.sh)
ots
</pre>

Copy and paste in Firefox or Chrome the link that appears on the terminal when OTSDAQ has started.




##  Setup file example
```sh
echo # This script is intended to be sourced.

sh -c "[ `ps $$ | grep bash | wc -l` -gt 0 ] || { echo 'Please switch to the bash shell before running the otsdaq-demo.'; exit; }" || exit

echo -e "setup [275]  \t ======================================================"
echo -e "setup [275]  \t Initially your products path was PRODUCTS=${PRODUCTS}"

unsetup_all

export OTSDAQ_HOME=$PWD
#export PRODUCTS=$OTSDAQ_HOME/products
export PRODUCTS=/cvmfs/fermilab.opensciencegrid.org/products/artdaq/

source ${PRODUCTS}/setup

setup mrb
setup git
source ${OTSDAQ_HOME}/localProducts_otsdaq_*/setup

#source mrbSetEnv
mrbsetenv
echo -e "setup [275]  \t Now your products path is PRODUCTS=${PRODUCTS}"
echo

# Setup environment when building with MRB (As there's no setupARTDAQOTS file)

export CETPKG_INSTALL=${PRODUCTS}
export CETPKG_J=`nproc`

export OTS_MAIN_PORT=2015

export USER_DATA="${OTSDAQ_HOME}/srcs/otsdaq_cmsburninbox/DataDefault"
export ARTDAQ_DATABASE_URI="filesystemdb://${OTSDAQ_HOME}/srcs/otsdaq_cmsburninbox/databases/filesystemdb/2023_10_Default_db"
export OTSDAQ_DATA="/data/CMSBurninBox"

##### BURNIN BOX CONFIGURATION ##########
export BURNINBOX_CONFIGURATION_FILE="${OTSDAQ_HOME}/srcs/otsdaq_cmsburninbox/BurninBox/configuration/BurninBoxConfiguration_Default.xml"
export OTSDAQ_CMSBURNINBOX_LIB=`echo ${MRB_BUILDDIR}/otsdaq_cmsburninbox/slf7*/lib`
export MODULE_NAMES_FILE="/tmp/ModuleNames.cfg"

##### OUTERTRACKER CONFIGURATION ##########
export PH2ACF_BASE_DIR=${OTSDAQ_CMSTRACKER_DIR}/otsdaq-cmstracker/Ph2_ACF
export ACFSUPERVISOR_ROOT=${OTSDAQ_CMSTRACKER_DIR}/otsdaq-cmstracker/ACFSupervisor
export Protobuf_DIRS=${OTSDAQ_CMSTRACKER_DIR}/protobuf

#########
# Flags #
#########
export HttpFlag='-D__HTTP__'
export ZmqFlag='-D__ZMQ__'
export USBINSTFlag='-D__USBINST__'
export Amc13Flag='-D__AMC13__'
export AntennaFlag='-D__ANTENNA__'
export UseRootFlag='-D__USE_ROOT__'
export MultiplexingFlag='-D__MULTIPLEXING__'
export EuDaqFlag='-D__EUDAQ__'
################
# Compilations #
################

# Stand-alone application, without data streaming
#        export CompileForHerd=false
#        export CompileForShep=false

# Stand-alone application, with data streaming
export CompileForHerd=true
export CompileForShep=true

# Herd application
# export CompileForHerd=true
# export CompileForShep=false

# Shep application
# export CompileForHerd=false
# export CompileForShep=true

# Compile with EUDAQ libraries
export CompileWithEUDAQ=false

echo -e "setup [275]  \t Now your user data path is USER_DATA \t\t = ${USER_DATA}"
echo -e "setup [275]  \t Now your database path is ARTDAQ_DATABASE_URI \t = ${ARTDAQ_DATABASE_URI}"
echo -e "setup [275]  \t Now your output data path is OTSDAQ_DATA \t = ${OTSDAQ_DATA}"
echo

alias rawEventDump="art -c /home/modtest/Programming/otsdaq/srcs/otsdaq/artdaq-ots/ArtModules/fcl/rawEventDump.fcl"
alias kx='StartOTS.sh -k'

echo
echo -e "setup [275]  \t Now use 'ots --wiz' to configure otsdaq"
echo -e "setup [275]  \t  	Then use 'ots' to start otsdaq"
echo -e "setup [275]  \t  	Or use 'ots --help' for more options"
echo
echo -e "setup [275]  \t     use 'kx' to kill otsdaq processes"
echo

#setup ninja v1_8_2
#setup nlohmann_json v3_9_0b -q e19:prof


#setup ninja generator
#============================
ninjaver=`ups list -aKVERSION ninja|sort -V|tail -1|sed 's|"||g'`
setup ninja $ninjaver
alias mb='pushd $MRB_BUILDDIR; ninja -j$CETPKG_J; popd'
alias mbb='mrb b --generator ninja'
alias mz='mrb z; mrbsetenv; mrb b --generator ninja'
alias mt='pushd $MRB_BUILDDIR;ninja -j$CETPKG_J;CTEST_PARALLEL_LEVEL=${CETPKG_J} ninja -j$CETPKG_J test;popd'
alias mtt='mrb t --generator ninja'
alias mi='pushd $MRB_BUILDDIR; ninja -j$CETPKG_J install;popd'
alias mii='mrb i --generator ninja'
```



