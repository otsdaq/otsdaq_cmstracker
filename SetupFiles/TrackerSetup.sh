export CONFIGURATION_NAME=Default
#export PROJECT_HOME=$(echo $PWD | sed -e 's|/user||')
export PROJECT_HOME=$PWD
export USER_DATA_HOME=${PROJECT_HOME}/otsdaq-cms-burninbox-data/${CONFIGURATION_NAME}
export SPACK_HOME=${PROJECT_HOME}/spack
export OTS_MAIN_PORT=2015

export SPACK_DISABLE_LOCAL_CONFIG=true 
source ${SPACK_HOME}/spack/share/spack/setup-env.sh

#This is for users 
spack env activate ots
#This is for developers
#spack env activate .

#return
export MRB_SOURCE=$PROJECT_HOME

##### DATA DIRECTORY CONFIGURATION ##########
#OTSDAQ_DATA is the dirctory where all histograms are stored!!
export OTSDAQ_DATA=${PROJECT_HOME}/OtsdaqData/BurninBoxOtsdaqData
mkdir -p ${OTSDAQ_DATA}

##### OTSDAQ MAIN ENVIRONMENT VARIABLES CONFIGURATION ##########
#USER_DATA is the directory where RunNumber, Logs and other user informations are stored
export USER_DATA=${USER_DATA_HOME}/UserData
#ARTDAQ_DATABASE_URI is the variable pointing to the USER database
export ARTDAQ_DATABASE_URI=filesystemdb://${USER_DATA_HOME}/Database

##### BURNIN BOX CONFIGURATION ##########
#Each center has its own specific configuration for the sensors on the controller - NCP
export BURNINBOX_CONFIGURATION_FILE=${USER_DATA_HOME}/HardwareConfiguration/BurninBoxConfiguration_${CONFIGURATION_NAME}.xml
export MODULE_NAMES_FILE=${USER_DATA_HOME}/tmp/ModuleNames.cfg

##### OUTERTRACKER CONFIGURATION ##########
#THIS IS USED ONLY FOR DEVELOP VERSION
export PH2ACF_BASE_DIR=${PROJECT_HOME}/otsdaq-cmstracker/otsdaq-cmstracker/Ph2_ACF
#export PH2ACF_BASE_DIR=${OTSDAQ_CMSTRACKER_DIR}/otsdaq-cmstracker/Ph2_ACF
export ACFSUPERVISOR_ROOT=${OTSDAQ_CMSTRACKER_DIR}/otsdaq-cmstracker/ACFSupervisor
export OTSDAQ_RESULTS_FOLDER=${OTSDAQ_DATA}/../Ph2_ACF_Results

ln -fs ${OTSDAQ_CMSBURNINBOX_DIR}/UserWebGUI ${OTSDAQ_UTILITIES_DIR}/WebGUI/CMSBurninBoxWebPath

echo -e "setup [275]  \t Now your user data path is USER_DATA \t\t = ${USER_DATA}"
echo -e "setup [275]  \t Now your database path is ARTDAQ_DATABASE_URI \t = ${ARTDAQ_DATABASE_URI}"
echo -e "setup [275]  \t Now your output data path is OTSDAQ_DATA \t = ${OTSDAQ_DATA}"
echo
echo
echo -e "setup [275]  \t Now use 'ots --wiz' to configure otsdaq"
echo -e "setup [275]  \t Then use 'ots' to start otsdaq"
echo -e "setup [275]  \t Or use 'ots --help' for more options"
echo
echo -e "setup [275]  \t Use 'kx' to kill otsdaq processes"
echo

alias kx='ots -k'

#setup git user info
#============================
export GIT_SSH_COMMAND="ssh -i ~/.ssh/id_`klist|grep "Default principal"|cut -d: -f2|sed 's/@FNAL.GOV//g;s/ //g'`_rsa"
EMAIL_ADDR=`klist|grep "Default principal"|cut -d: -f2|sed 's/ //g'`
git config --global user.email $EMAIL_ADDR
echo "Found git email as ${EMAIL_ADDR}"
